ِ<?php include 'inc/header.php';
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->


        <section class="content">

            <h2 class="page-header">User Management</h2>
            <div class="row">
                <div class="col-md-12">
                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab" style="font-weight: 600">Active Users</a></li>
                            <li><a href="#tab_2" data-toggle="tab" style="font-weight: 600">Block Users</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">

                                <div class="box">

                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>

                                            <tr>
                                                <th>ID</th>
                                                <th>User Name</th>
                                                <th>Member Type</th>
                                                <th>Company Name</th>
                                                <th>Company Status</th>
                                                <th style="width: 100px">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            //print_r($countries);
                                            foreach ($active_users as $active) {
                                                //print_r($values);

                                                    ?>
                                                    <tr>
                                                        <td><?php echo $active->user_id; ?></td>
                                                        <td><?= $active->user_name; ?></td>
                                                        <td style='font-style: italic;'><?= $active->member_type; ?></td>
                                                            <td style='font-style: italic;'><?= $active->company_name; ?></td>
                                                            <td style='font-style: italic;' ><?php
                                                            if($active->status == 1){
                                                                echo"Verified";
                                                            }else{
                                                                echo"Unverfied";
                                                            }
                                                             ?></td>



                                                        <td style="width: 100px">

                                                            <a href="<?php echo base_url()?>index.php/users/do_bann_user/<?php echo $active->user_id?>" class="btn btn-sm btn-danger" style="float:right;" ><span > Block </span></a>
                                                        </td>
                                                    </tr>
                                                    <?php

                                            }
                                            ?>
                                        </tbody>
                                        </table>
                                    </div><!-- /.box-body -->
                                </div><!-- /.box -->

                            </div><!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">
                                <div class="box">

                                <div class="box-body table-responsive">
                                    <table id="table2" class="table table-bordered table-striped">
                                        <thead>

                                            <tr>
                                                <th>ID</th>
                                                <th>User Name</th>
                                                <th>Member Type</th>
                                                <th>Company Name</th>
                                                <th>Company Status</th>
                                                <th style="width: 100px">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            //print_r($countries);
                                            foreach ($banned_users as $banned) {
                                                //print_r($values);

                                                    ?>
                                                    <tr>
                                                        <td><?php echo $banned->user_id; ?></td>
                                                        <td><?= $banned->user_name; ?></td>
                                                        <td style='font-style: italic;'><?= $banned->member_type; ?></td>
                                                            <td style='font-style: italic;'><?= $banned->company_name; ?></td>
                                                            <td style='font-style: italic;'><?php
                                                            if($banned->status == 0){
                                                                echo"Unverfied";
                                                            }else{
                                                                echo"Verfied";
                                                            }
                                                             ?></td>



                                                        <td style="width: 100px">

                                                            <a href="<?php echo base_url()?>index.php/users/un_bann_user/<?php echo $banned->user_id?>" class="btn btn-sm btn-danger" style="float:right;" ><span > Un Block </span></a>
                                                        </td>
                                                    </tr>
                                                    <?php

                                            }
                                            ?>
                                        </tbody>
                                        </table>

                                    </div><!-- /.box-body -->
                                </div><!-- /.box -->
                            </div><!-- /.tab-pane -->
                        </div><!-- /.tab-content -->
                    </div><!-- nav-tabs-custom -->
                </div><!-- /.col -->


            </div> <!-- /.row -->ٖ
        </section>

    </aside>

</div>
<script>
    document.getElementById("company_tab").className = "treeview active";
    document.getElementById("user_list").className = "active";
</script>
<?php
include 'inc/footer.php';
include 'inc/modals.php';
?>