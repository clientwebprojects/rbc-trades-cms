ِ<?php include 'inc/header.php';
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">

        <section class="content">
            <a href="<?php echo base_url()?>index.php/system/add_new_currency/" class="btn btn-primary pull-right">Add Currency</a>
            <h2 class="page-header">Currency Management</h2>
            <div class="row">
                <div class="col-md-12">
                    <!-- Custom Tabs -->
                      <div class="box">

                                    <div class="box">

                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Currency Name</th>
                                                <th>Currency Code</th>
                                                <th>Currency Symbol</th>
                                                <th style="width: 100px">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($currency_listing as $values) {
                                                ?>

                                                <tr>
                                                    <td><?php echo $values->id; ?></td>
                                                    <td><?php echo $values->currency_name; ?></td>
                                                    <td><?php echo strtoupper($values->currency_code); ?></td>
                                                    <td><?php echo $values->currency_symbol; ?></td>
                                                    <td class="btn-group" style="width: 100px">
                                                        <a class="btn btn-xs btn-primary update_country" href="<?php echo base_url()?>index.php/system/edit_currency/<?php echo $values->id?>">Edit</a>
                                                        <a class="btn btn-xs btn-danger" href="<?php echo base_url()?>index.php/system/delete_currency/<?php echo $values->id?>" onclick="return confirm('Are you sure to Delete?')">Delete</a>
                                                    </td>

                                                </tr>

                                                <?php
                                            }
                                            ?>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>

                                            </tr>
                                        </tfoot>
                                       </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                </div><!-- /.col -->


            </div> <!-- /.row -->ٖ
            </div>
        </section>

    </aside>

</div>
<script>
    document.getElementById("website_tab").className = "treeview active";
    document.getElementById("currency").className = "active";
</script>
<?php
include 'inc/footer.php';
?>