<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= base_url() ?>img/avatar5.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Hello, Admin</p>

                <a href="#">Administrator</a>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li>
                <a href="<?= base_url() ?>">
                    <i class="fa fa-home"></i> <span>Dashboard</span>
                </a>
            </li>
            <li id="website_tab" class="treeview">
                <a href="#">
                    <i class="fa fa-gears"></i>
                    <span>Website</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="category_management"><a href="<?= base_url() ?>index.php/system/category_listing"><i class="fa fa-angle-double-right"></i> Category Management</a></li>
                    <li id="currency"><a href="<?= base_url() ?>index.php/system/currency_listing"><i class="fa fa-angle-double-right"></i> Currency</a></li>
                    <li id="country"><a href="<?= base_url() ?>index.php/system/country_listing"><i class="fa fa-angle-double-right"></i> Country</a></li>
                    <li id="departments"><a href="<?= base_url() ?>index.php/system/department_listing"> <i class="fa fa-angle-double-right"></i> <span>Departments</span> </a> </li>
                    <li id="btype_management"><a href="<?= base_url() ?>index.php/system/business_type_listing"><i class="fa fa-angle-double-right"></i> Business Type</a></li>
                    <li id="pterm_management"><a href="<?= base_url() ?>index.php/system/payment_term_listing"><i class="fa fa-angle-double-right"></i> Payment Terms</a></li>
                    <li id="p_frequency"><a href="<?= base_url() ?>index.php/system/purchasing_frequency_listing"><i class="fa fa-angle-double-right"></i> <span>Purchasing Frequency</span> </a> </li>
                    <li id="annual_sale_vol"><a href="<?= base_url() ?>index.php/system/sales_volume_listing"><i class="fa fa-angle-double-right"></i> <span>Annual Sales Volume List</span> </a> </li>
                    <li id="export_percent"><a href="<?= base_url() ?>index.php/system/export_percentage_listing"><i class="fa fa-angle-double-right"></i> <span>Export Percentage List</span> </a> </li>
                    <li id="office_sizes"><a href="<?= base_url() ?>index.php/system/office_size_listing"><i class="fa fa-angle-double-right"></i> <span>Office Size List</span> </a> </li>
                    <li id="no_employee"><a href="<?= base_url() ?>index.php/system/employees_listing"><i class="fa fa-angle-double-right"></i> <span>No of Employees List</span> </a> </li>
                    <li id="security_question"><a href="<?= base_url() ?>index.php/system/security_question_listing"> <i class="fa fa-angle-double-right"></i> <span>Security Question</span> </a> </li>
                </ul>

            </li>
            <li id="company_tab" class="treeview">
                <a href="#">
                    <i class="fa fa-gears"></i>
                    <span>Company Management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="user_list"><a href="<?= base_url() ?>index.php/users/users_listing"><i class="fa fa-angle-double-right"></i> User List</a></li>
                    <li id="company_list"><a href="<?= base_url() ?>index.php/users/company_listing"><i class="fa fa-angle-double-right"></i> Company List</a></li>
<!--                    <li><a href="<?= base_url() ?>index.php/system/news_listing"><i class="fa fa-angle-double-right"></i> News Management</a></li>-->
                </ul>

            </li>
            <li id="product_tab" class="treeview">
                <a href="#">
                    <i class="fa fa-shopping-cart"></i>
                    <span>Product/Service</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="admin_product"><a href="<?= base_url() ?>index.php/system/product_listing_admin"><i class="fa fa-angle-double-right"></i> Admin Products</a></li>
                    <li id="company_product"><a href="<?= base_url() ?>index.php/system/product_listing"><i class="fa fa-angle-double-right"></i> Company Products</a></li>
                    <li id="company_service"><a href="<?= base_url() ?>index.php/system/service_listing"><i class="fa fa-angle-double-right"></i> Company Services</a></li>
                    <li></li>
                </ul>

            </li>
             <li id="membership_tab" class="treeview">
                <a href="#">
                    <i class="fa fa-star"></i>
                    <span>RBC Membership</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="membership_listing"><a href="<?= base_url() ?>index.php/system/membership"><i class="fa fa-angle-double-right"></i> Membership</a></li>
                    <li id="membership_package_listing"><a href="<?= base_url() ?>index.php/system/membership_package"><i class="fa fa-angle-double-right"></i> Membership Package</a></li>

                    <li></li>
                </ul>

            </li>
            <li id="banner_tab" class="treeview">
                <a href="#">
                    <i class="fa fa-picture-o"></i>
                    <span>Banner Management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="home_slider"><a href="<?= base_url() ?>index.php/system/slider_banner"><i class="fa fa-angle-double-right"></i> Home Slider</a></li>
                    <li id="cat_banner"><a href="<?= base_url() ?>index.php/system/category_banner"><i class="fa fa-angle-double-right"></i> Banners in Category</a></li>

                    <li></li>
                </ul>

            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

