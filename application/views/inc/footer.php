<script src="<?= base_url() ?>js/jquery.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/bootstrap-select.js"></script>
<script src="<?= base_url() ?>js/jquery-2.1.4.min.js"></script>
<script src="<?= base_url() ?>js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.p_listing').click(function() {
      $("li.active").removeClass("active");
      $(this).addClass('active');
});
    });
</script>
<script>
    $(document).ready(function() {
        $("#forProduct").click(function() {
            $('#catService').hide();
            $('#catProduct').show();
        });
        $("#forService").click(function() {
            $('#catProduct').hide();
            $('#catService').show();
        });
        $(".add_new_frequency_btn").click(function() {
            //$("#emp_number").val($(this).data('id'));
            $('#addPurchasingfrequencyForm').modal('show');
        });
         $(".add_new_country_btn").click(function() {
            //$("#emp_number").val($(this).data('id'));
            $('#addCountryForm').modal('show');
        });
        $(".add_new_payment_type_btn").click(function() {
            //$("#emp_number").val($(this).data('id'));
            $('#addPaymentTermForm').modal('show');
        });
        $(".add_new_department_btn").click(function() {
            //$("#emp_number").val($(this).data('id'));
            $('#addDepartmentForm').modal('show');
        });
        $(".add_new_business_type_btn").click(function() {
            //$("#emp_number").val($(this).data('id'));
            $('#addBusinessTypeForm').modal('show');
        });
        $(".add_new_news_btn").click(function() {
            //$("#emp_number").val($(this).data('id'));
            $('#addNewsForm').modal('show');
        });
        $(".add_new_question_btn").click(function() {
            //$("#emp_number").val($(this).data('id'));
            $('#addQuestionForm').modal('show');
        });
        $(".add_new_package_btn").click(function() {
            $('#add_new_package_form').modal('show');
        });
        $(".update_security_question").click(function() {

            $("#security_id").val($(this).data('id'));
            $("#test_question").val($(this).data('todo').security_question);
            $('#update_security_question').modal('show');
        });
        $(".update_country").click(function() {

            $("#country_id").val($(this).data('id'));
            $("#country_name").val($(this).data('todo').country_name);
            $('#editCountry').modal('show');
        });
        $(".update_purchasing_frequency").click(function() {

            $("#frequency_id").val($(this).data('id'));
            $("#purchasing_frequency").val($(this).data('todo').purchasing_frequency);
            $('#editPurchasingFrequency').modal('show');
        });
        $(".update_department").click(function() {

            $("#department_id").val($(this).data('id'));
            $("#department_name").val($(this).data('todo').department_name);
            $('#editDepartment').modal('show');
        });
        $(".update_business_type").click(function() {

            $("#business_type_id").val($(this).data('id'));
            $("#business_type_name").val($(this).data('todo').business_type_name);
            $('#update_business_type').modal('show');
        });
        $(".update_payment").click(function() {

            $("#p_term_id").val($(this).data('id'));
            $("#p_term_description").val($(this).data('todo').p_term_description);
            $('#editPaymentType').modal('show');
        });
        $(".edit-category-btn").click(function() {
            //$("#emp_number").val($(this).data('id'));
            console.log(catId);
            console.log(catName);
             $("#category_id").val(catId);
             $("#category_name").val(catName);
             $('#editCategoty').modal('show');
        });
        $(".add_new_sales_btn").click(function() {
            $('#addsalesvolume').modal('show');
        });
        $(".update_sales_volume").click(function() {
            $("#sales_id").val($(this).data('id'));
            $("#sales_volume").val($(this).data('todo').description);
            $('#editsalesvolume').modal('show');
        });
         $(".add_export_btn").click(function() {
            $('#addExportPercentage').modal('show');
        });
        $(".update_export_percentage").click(function() {
            $("#export_id").val($(this).data('id'));
            $("#export_percentage").val($(this).data('todo').description);
            $('#editexportpercentage').modal('show');
        });
        $(".add_office_btn").click(function() {
            $('#addofficesize').modal('show');
        });
        $(".update_office_size").click(function() {
            $("#office_id").val($(this).data('id'));
            $("#office_size_d").val($(this).data('todo').description);
            $('#editofficesize').modal('show');
        });
        $(".add_employees_btn").click(function() {
            $('#addemployees').modal('show');
        });
        $(".update_employees").click(function() {
            $("#employees_id").val($(this).data('id'));
            $("#employees_d").val($(this).data('todo').description);
            $('#editEmployees').modal('show');
        });
        $(".add_staff_btn").click(function() {
            $('#addstaff').modal('show');
        });
        $(".update_staff").click(function() {
            $("#staff_id").val($(this).data('id'));
            $("#staff_d").val($(this).data('todo').description);
            $('#editStaff').modal('show');
        });
        $(".update_membership_package").click(function() {
            $("#pack_id").val($(this).data('id'));
            $("#package_fee").val($(this).data('todo').package_fee);
            $("#currency_id").val($(this).data('todo').currency_id);
            $("#package_duration").val($(this).data('todo').package_duration);
            $('#edit_package_form').modal('show');
        });
    });


</script>
<script type="text/javascript">
            $(function() {
                $('#example1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": false,
                    "bAutoWidth": false
                });
            });
        </script>
        <script type="text/javascript">
//            $(function() {
//                $("#table3").dataTable();
//                $('#example2').dataTable({
//                    "bPaginate": true,
//                    "bLengthChange": false,
//                    "bFilter": false,
//                    "bSort": true,
//                    "bInfo": true,
//                    "bAutoWidth": false
//                });
//            });
        </script>
<script src="<?= base_url() ?>js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/AdminLTE/app.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
</body>
</html>