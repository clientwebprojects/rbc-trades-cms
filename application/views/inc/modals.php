<div class="modal fade" id="addCountryForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  name="add_country" class="form-horizontal" method="POST" action="">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add new Country</h4>
                </div>

                <div class="modal-body panel-body">
                    <div class="form-group" >
                        <label class="col-sm-3 control-label" style="font-weight: bold;">Name</label>
                        <div class="col-sm-6">
                            <input name="country_name" type="text" class="form-control input-medium" value="" required/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                    <button type="submit" value="add_country" name="add_country" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="addBusinessTypeForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  name="add_business_type" class="form-horizontal" method="POST" action="">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add new Business Type</h4>
                </div>

                <div class="modal-body panel-body">
                    <div class="form-group" >
                        <label class="col-sm-3 control-label" style="font-weight: bold;">Business Name</label>
                        <div class="col-sm-6">
                            <input name="business_type_name" type="text" class="form-control input-medium" value="" required/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                    <button type="submit" value="add_business_type" name="add_business_type" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="addQuestionForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  name="add_question" class="form-horizontal" method="POST" action="">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add new Question</h4>
                </div>

                <div class="modal-body panel-body">
                    <div class="form-group" >
                        <label class="col-sm-3 control-label" style="font-weight: bold;">Security Question</label>
                        <div class="col-sm-8">
                            <input name="security_question" type="text" class="form-control input-large" value="" required/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                    <button type="submit" value="add_security_question" name="add_security_question" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="addDepartmentForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  name="add_department" class="form-horizontal" method="POST" action="">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add New Department</h4>
                </div>

                <div class="modal-body panel-body">
                    <div class="form-group" >
                        <label class="col-sm-3 control-label" style="font-weight: bold;">Department Name</label>
                        <div class="col-sm-6">
                            <input name="department_name" type="text" class="form-control input-medium" value="" required/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                    <button type="submit" value="add_department" name="add_department" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="addPaymentTermForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  name="add_payment_type" class="form-horizontal" method="POST" action="">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add new Payment Type</h4>
                </div>

                <div class="modal-body panel-body">
                    <div class="form-group" >
                        <label class="col-sm-3 control-label" style="font-weight: bold;">Name</label>
                        <div class="col-sm-6">
                            <input name="p_term_description" type="text" class="form-control input-medium" value="" required/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                    <button type="submit" value="add_payment_term" name="add_payment_term" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="update_business_type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form  name="update_business_type" class="form-horizontal" method="POST" action="">
                    <input type="hidden" id="business_type_id" name="business_type_id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Business Type</h4>
                    </div>
                    <div class="modal-body panel-body">

                        <div class="form-group" >
                            <label class="col-sm-3 control-label" style="font-weight: bold;">Business Name</label>
                            <div class="col-sm-6">
                                <input id="business_type_name" name="business_type_name" type="text" class="form-control input-medium" required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                        <button type="update" value="update" name="edit_business_type" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="update_security_question" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form  name="edit_security_question" class="form-horizontal" method="POST" action="">
                    <input type="hidden" id="security_id" name="id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Security Question</h4>
                    </div>
                    <div class="modal-body panel-body">

                        <div class="form-group" >
                            <label class="col-sm-3 control-label" style="font-weight: bold;">Question</label>
                            <div class="col-sm-6">
                                <input id="test_question" name="security_question" type="text" class="form-control input-large" required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                        <button type="update" value="update" name="edit_security_question" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editCountry" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form  name="edit_country" class="form-horizontal" method="POST" action="">
                    <input type="hidden" id="country_id" name="country_id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Country</h4>
                    </div>
                    <div class="modal-body panel-body">

                        <div class="form-group" >
                            <label class="col-sm-3 control-label" style="font-weight: bold;">Name</label>
                            <div class="col-sm-6">
                                <input id="country_name" name="country_name" type="text" class="form-control input-medium" required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                        <button type="update" value="update" name="edit_country" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editDepartment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form  name="edit_department" class="form-horizontal" method="POST" action="">
                    <input type="hidden" id="department_id" name="id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Department</h4>
                    </div>
                    <div class="modal-body panel-body">

                        <div class="form-group" >
                            <label class="col-sm-3 control-label" style="font-weight: bold;">Department Name</label>
                            <div class="col-sm-6">
                                <input id="department_name" name="department_name" type="text" class="form-control input-medium" required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                        <button type="update" value="update" name="edit_department" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editPaymentType" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form  name="edit_payment_type" class="form-horizontal" method="POST" action="">
                    <input type="hidden" id="p_term_id" name="p_term_id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Country</h4>
                    </div>
                    <div class="modal-body panel-body">

                        <div class="form-group" >
                            <label class="col-sm-3 control-label" style="font-weight: bold;">Name</label>
                            <div class="col-sm-6">
                                <input id="p_term_description" name="p_term_description" type="text" class="form-control input-medium" required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                        <button type="update" value="update" name="edit_payment" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editCategoty" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form  name="edit_country" class="form-horizontal" method="POST" action="">
                    <input type="hidden" id="category_id" name="category_id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Category</h4>
                    </div>
                    <div class="modal-body panel-body">

                        <div class="form-group" >
                            <label class="col-sm-3 control-label" style="font-weight: bold;">Name</label>
                            <div class="col-sm-6">
                                <input id="category_name" name="category_name" type="text" class="form-control input-medium" required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                        <button type="update" value="update" name="edit_category" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addPurchasingfrequencyForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  name="add_purchasing_frequency" class="form-horizontal" method="POST" action="">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add new Purchasing Frequency</h4>
                </div>

                <div class="modal-body panel-body">
                    <div class="form-group" >
                        <label class="col-sm-3 control-label" style="font-weight: bold;">Purchasing Frequency</label>
                        <div class="col-sm-6">
                            <input name="purchasing_frequency" type="text" class="form-control input-medium" value="" required/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                    <button type="submit" value="add_purchasing_frequency" name="add_purchasing_frequency" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="editPurchasingFrequency" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form  name="edit_payment_type" class="form-horizontal" method="POST" action="">
                    <input type="hidden" id="frequency_id" name="id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Purchasing Frequency</h4>
                    </div>
                    <div class="modal-body panel-body">

                        <div class="form-group" >
                            <label class="col-sm-3 control-label" style="font-weight: bold;">Purchasing_Frequency</label>
                            <div class="col-sm-6">
                                <input id="purchasing_frequency" name="purchasing_frequency" type="text" class="form-control input-medium" required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                        <button type="update" value="update" name="edit_purchasing_frequency" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addsalesvolume" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  name="add_sales_volume" class="form-horizontal" method="POST" action="">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add new Annual Sales Volume</h4>
                </div>

                <div class="modal-body panel-body">
                    <div class="form-group" >
                        <label class="col-sm-3 control-label" style="font-weight: bold;">Annual Sales Volume</label>
                        <div class="col-sm-6">
                            <input name="description" type="text" class="form-control input-medium" value="" required/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                    <button type="submit" value="add" name="add_sales_volume" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="editsalesvolume" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form  name="editSAlesVolume" class="form-horizontal" method="POST" action="">
                    <input type="hidden" id="sales_id" name="id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Sales Volume</h4>
                    </div>
                    <div class="modal-body panel-body">

                        <div class="form-group" >
                            <label class="col-sm-3 control-label" style="font-weight: bold;">Edit Name</label>
                            <div class="col-sm-6">
                                <input id="sales_volume" name="description" type="text" class="form-control input-medium" required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                        <button type="update" value="update" name="edit_sales_volume" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addExportPercentage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  name="add_export_percentage" class="form-horizontal" method="POST" action="">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add new Export Percentage</h4>
                </div>

                <div class="modal-body panel-body">
                    <div class="form-group" >
                        <label class="col-sm-3 control-label" style="font-weight: bold;">Export Percentage</label>
                        <div class="col-sm-6">
                            <input name="description" type="text" class="form-control input-medium" value="" required/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                    <button type="submit" value="add" name="add_export_percentage" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="editexportpercentage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form  name="editSAlesVolume" class="form-horizontal" method="POST" action="">
                    <input type="hidden" id="export_id" name="id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Export Percentage</h4>
                    </div>
                    <div class="modal-body panel-body">

                        <div class="form-group" >
                            <label class="col-sm-3 control-label" style="font-weight: bold;">Edit</label>
                            <div class="col-sm-6">
                                <input id="export_percentage" name="description" type="text" class="form-control input-medium" required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                        <button type="update" value="update" name="edit_export_percentage" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addofficesize" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  name="add_office_size" class="form-horizontal" method="POST" action="">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add new Office Size</h4>
                </div>

                <div class="modal-body panel-body">
                    <div class="form-group" >
                        <label class="col-sm-3 control-label" style="font-weight: bold;">Office Size</label>
                        <div class="col-sm-6">
                            <input name="description" type="text" class="form-control input-medium" value="" required/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                    <button type="submit" value="add" name="add_office_size" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="editofficesize" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form  name="edit_office_size" class="form-horizontal" method="POST" action="">
                    <input type="hidden" id="office_id" name="id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Export Percentage</h4>
                    </div>
                    <div class="modal-body panel-body">

                        <div class="form-group" >
                            <label class="col-sm-3 control-label" style="font-weight: bold;">Edit</label>
                            <div class="col-sm-6">
                                <input id="office_size_d" name="description" type="text" class="form-control input-medium" required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                        <button type="update" value="update" name="edit_office_size" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addemployees" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  name="add_office_size" class="form-horizontal" method="POST" action="">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add New Employees</h4>
                </div>

                <div class="modal-body panel-body">
                    <div class="form-group" >
                        <label class="col-sm-3 control-label" style="font-weight: bold;">NO of Employees</label>
                        <div class="col-sm-6">
                            <input name="description" type="text" class="form-control input-medium" value="" required/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                    <button type="submit" value="add" name="add_employees" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="editEmployees" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form  name="edit_no_of_employees" class="form-horizontal" method="POST" action="">
                    <input type="hidden" id="employees_id" name="id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit NO of Employees</h4>
                    </div>
                    <div class="modal-body panel-body">

                        <div class="form-group" >
                            <label class="col-sm-3 control-label" style="font-weight: bold;">Edit</label>
                            <div class="col-sm-6">
                                <input id="employees_d" name="description" type="text" class="form-control input-medium" required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                        <button type="update" value="update" name="edit_employees" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addstaff" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  name="add_staff" class="form-horizontal" method="POST" action="">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add New Staff Record</h4>
                </div>

                <div class="modal-body panel-body">
                    <div class="form-group" >
                        <label class="col-sm-3 control-label" style="font-weight: bold;">NO of Staff</label>
                        <div class="col-sm-6">
                            <input name="description" type="text" class="form-control input-medium" value="" required/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                    <button type="submit" value="add" name="add_staff" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="editStaff" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form  name="edit_no_of_staff" class="form-horizontal" method="POST" action="">
                    <input type="hidden" id="staff_id" name="id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit NO of Staff Record</h4>
                    </div>
                    <div class="modal-body panel-body">

                        <div class="form-group" >
                            <label class="col-sm-3 control-label" style="font-weight: bold;">Edit</label>
                            <div class="col-sm-6">
                                <input id="staff_d" name="description" type="text" class="form-control input-medium" required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                        <button type="update" value="update" name="edit_staff" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!--Membership Package-->

<div class="modal fade" id="add_new_package_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  name="add_membership_package" class="form-horizontal" method="POST" action="">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add New Package</h4>
                </div>

                <div class="modal-body panel-body">
                    <div class="form-group" >
                        <label class="col-sm-3 control-label" style="font-weight: bold;">Package Fee</label>
                        <div class="col-sm-6">
                            <input name="package_fee" type="text" class="form-control input-medium" value="" required/>
                        </div>
                    </div>
                    <div class="form-group" >
                        <label class="col-sm-3 control-label" style="font-weight: bold;">Select Currency</label>
                        <div class="col-md-6">
                            <select name="currency_id" class="form-control" required>
                            <?php
                            foreach ($currency as $currency){
                            ?>
                            <option value="<?=$currency->id ?>"><?php echo "(".$currency->currency_symbol.") ".$currency->currency_name ?></option>
                            <?php } ?>
                        </select>
                        </div>
                    </div>
                    <div class="form-group" >
                        <label class="col-sm-3 control-label" style="font-weight: bold;">Package Duration</label>
                        <div class="col-sm-6">
                            <input name="package_duration" type="text" class="form-control input-medium" value="" placeholder="eg: For 1 Year" required/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                    <button type="submit" value="add_membership_package" name="add_membership_package" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="edit_package_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  name="edit_membership_package" class="form-horizontal" method="POST" action="">
                <input type="hidden" id="pack_id" name="pack_id" value=""/>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Package Detail</h4>
                </div>

                <div class="modal-body panel-body">
                    <div class="form-group" >
                        <label class="col-sm-3 control-label" style="font-weight: bold;">Package Fee</label>
                        <div class="col-sm-6">
                            <input id="package_fee" name="package_fee" type="text" class="form-control input-medium" value="" required/>
                        </div>
                    </div>
                    <div class="form-group" >
                        <label class="col-sm-3 control-label" style="font-weight: bold;">Select Currency</label>
                        <div class="col-md-6">
                            <select id="currency_id" name="currency_id" class="form-control" required>
                            <?php
                            foreach ($currency2 as $c){
                            ?>
                            <option value="<?=$c->id ?>"><?php echo "(".$c->currency_symbol.") ".$c->currency_name ?></option>
                            <?php } ?>
                        </select>
                        </div>
                    </div>
                    <div class="form-group" >
                        <label class="col-sm-3 control-label" style="font-weight: bold;">Package Duration</label>
                        <div class="col-sm-6">
                            <input id="package_duration" name="package_duration" type="text" class="form-control input-medium" value="" placeholder="eg: For 1 Year" required/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="padding: 10px 10px 10px;margin-top: 0;">

                    <button type="submit" value="edit_membership_package" name="edit_membership_package" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>