<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>RBC ADMIN PANEL</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<?= base_url() ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?= base_url() ?>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?= base_url() ?>css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="<?= base_url() ?>css/morris/morris.css" rel="stylesheet" type="text/css" />
         <link href="<?= base_url() ?>css/style.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="<?= base_url() ?>css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- fullCalendar -->
        <link href="<?= base_url() ?>css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="<?= base_url() ?>css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="<?= base_url() ?>css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?= base_url() ?>css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/bootstrap-select.css">
        <script src="<?= base_url() ?>js/ckeditor.js"></script>
        <script>

            // The instanceReady event is fired, when an instance of CKEditor has finished
            // its initialization.
            CKEDITOR.on('instanceReady', function (ev) {
                // Show the editor name and description in the browser status bar.
                document.getElementById('eMessage').innerHTML = 'Instance <code>' + ev.editor.name + '<\/code> loaded.';

                // Show this sample buttons.
                document.getElementById('eButtons').style.display = 'block';
            });

            function InsertHTML() {
                // Get the editor instance that we want to interact with.
                var editor = CKEDITOR.instances.editor1;
                var value = document.getElementById('htmlArea').value;

                // Check the active editing mode.
                if (editor.mode == 'wysiwyg')
                {
                    // Insert HTML code.
                    // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-insertHtml
                    editor.insertHtml(value);
                }
                else
                    alert('You must be in WYSIWYG mode!');
            }

            function InsertText() {
                // Get the editor instance that we want to interact with.
                var editor = CKEDITOR.instances.editor1;
                var value = document.getElementById('txtArea').value;

                // Check the active editing mode.
                if (editor.mode == 'wysiwyg')
                {
                    // Insert as plain text.
                    // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-insertText
                    editor.insertText(value);
                }
                else
                    alert('You must be in WYSIWYG mode!');
            }

            function SetContents() {
                // Get the editor instance that we want to interact with.
                var editor = CKEDITOR.instances.editor1;
                var value = document.getElementById('htmlArea').value;

                // Set editor contents (replace current contents).
                // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-setData
                editor.setData(value);
            }

            function GetContents() {
                // Get the editor instance that you want to interact with.
                var editor = CKEDITOR.instances.editor1;

                // Get editor contents
                // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-getData
                alert(editor.getData());
            }

            function ExecuteCommand(commandName) {
                // Get the editor instance that we want to interact with.
                var editor = CKEDITOR.instances.editor1;

                // Check the active editing mode.
                if (editor.mode == 'wysiwyg')
                {
                    // Execute the command.
                    // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-execCommand
                    editor.execCommand(commandName);
                }
                else
                    alert('You must be in WYSIWYG mode!');
            }

            function CheckDirty() {
                // Get the editor instance that we want to interact with.
                var editor = CKEDITOR.instances.editor1;
                // Checks whether the current editor contents present changes when compared
                // to the contents loaded into the editor at startup
                // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-checkDirty
                alert(editor.checkDirty());
            }

            function ResetDirty() {
                // Get the editor instance that we want to interact with.
                var editor = CKEDITOR.instances.editor1;
                // Resets the "dirty state" of the editor (see CheckDirty())
                // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-resetDirty
                editor.resetDirty();
                alert('The "IsDirty" status has been reset');
            }

            function Focus() {
                CKEDITOR.instances.editor1.focus();
            }

            function onFocus() {
                document.getElementById('eMessage').innerHTML = '<b>' + this.name + ' is focused </b>';
            }

            function onBlur() {
                document.getElementById('eMessage').innerHTML = this.name + ' lost focus';
            }

        </script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script>
            var base_url = "<?php echo base_url(); ?>";

        </script>
    </head>
    <body class="skin-black fixed">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="<?= base_url() ?>" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                RBC Admin Panel
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                        <!--                        <li class="dropdown messages-menu">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-envelope"></i>
                                                        <span class="label label-success">4</span>
                                                    </a>
                                                    <ul class="dropdown-menu">
                                                        <li class="header">You have 4 messages</li>
                                                        <li>
                                                             inner menu: contains the actual data
                                                            <ul class="menu">
                                                                <li> start message
                                                                    <a href="#">
                                                                        <div class="pull-left">
                                                                            <img src="img/avatar3.png" class="img-circle" alt="User Image"/>
                                                                        </div>
                                                                        <h4>
                                                                            Support Team
                                                                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                                                        </h4>
                                                                        <p>Why not buy a new awesome theme?</p>
                                                                    </a>
                                                                </li> end message
                                                                <li>
                                                                    <a href="#">
                                                                        <div class="pull-left">
                                                                            <img src="img/avatar2.png" class="img-circle" alt="user image"/>
                                                                        </div>
                                                                        <h4>
                                                                            AdminLTE Design Team
                                                                            <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                                                        </h4>
                                                                        <p>Why not buy a new awesome theme?</p>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <div class="pull-left">
                                                                            <img src="img/avatar.png" class="img-circle" alt="user image"/>
                                                                        </div>
                                                                        <h4>
                                                                            Developers
                                                                            <small><i class="fa fa-clock-o"></i> Today</small>
                                                                        </h4>
                                                                        <p>Why not buy a new awesome theme?</p>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <div class="pull-left">
                                                                            <img src="img/avatar2.png" class="img-circle" alt="user image"/>
                                                                        </div>
                                                                        <h4>
                                                                            Sales Department
                                                                            <small><i class="fa fa-clock-o"></i> Yesterday</small>
                                                                        </h4>
                                                                        <p>Why not buy a new awesome theme?</p>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <div class="pull-left">
                                                                            <img src="img/avatar.png" class="img-circle" alt="user image"/>
                                                                        </div>
                                                                        <h4>
                                                                            Reviewers
                                                                            <small><i class="fa fa-clock-o"></i> 2 days</small>
                                                                        </h4>
                                                                        <p>Why not buy a new awesome theme?</p>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="footer"><a href="#">See All Messages</a></li>
                                                    </ul>
                                                </li>
                                                 Notifications: style can be found in dropdown.less
                                                <li class="dropdown notifications-menu">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-warning"></i>
                                                        <span class="label label-warning">10</span>
                                                    </a>
                                                    <ul class="dropdown-menu">
                                                        <li class="header">You have 10 notifications</li>
                                                        <li>
                                                             inner menu: contains the actual data
                                                            <ul class="menu">
                                                                <li>
                                                                    <a href="#">
                                                                        <i class="ion ion-ios7-people info"></i> 5 new members joined today
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <i class="fa fa-warning danger"></i> Very long description here that may not fit into the page and may cause design problems
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <i class="fa fa-users warning"></i> 5 new members joined
                                                                    </a>
                                                                </li>

                                                                <li>
                                                                    <a href="#">
                                                                        <i class="ion ion-ios7-cart success"></i> 25 sales made
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <i class="ion ion-ios7-person danger"></i> You changed your username
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="footer"><a href="#">View all</a></li>
                                                    </ul>
                                                </li>
                                                 Tasks: style can be found in dropdown.less
                                                <li class="dropdown tasks-menu">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-tasks"></i>
                                                        <span class="label label-danger">9</span>
                                                    </a>
                                                    <ul class="dropdown-menu">
                                                        <li class="header">You have 9 tasks</li>
                                                        <li>
                                                             inner menu: contains the actual data
                                                            <ul class="menu">
                                                                <li> Task item
                                                                    <a href="#">
                                                                        <h3>
                                                                            Design some buttons
                                                                            <small class="pull-right">20%</small>
                                                                        </h3>
                                                                        <div class="progress xs">
                                                                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                                                <span class="sr-only">20% Complete</span>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </li> end task item
                                                                <li> Task item
                                                                    <a href="#">
                                                                        <h3>
                                                                            Create a nice theme
                                                                            <small class="pull-right">40%</small>
                                                                        </h3>
                                                                        <div class="progress xs">
                                                                            <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                                                <span class="sr-only">40% Complete</span>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </li> end task item
                                                                <li> Task item
                                                                    <a href="#">
                                                                        <h3>
                                                                            Some task I need to do
                                                                            <small class="pull-right">60%</small>
                                                                        </h3>
                                                                        <div class="progress xs">
                                                                            <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                                                <span class="sr-only">60% Complete</span>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </li> end task item
                                                                <li> Task item
                                                                    <a href="#">
                                                                        <h3>
                                                                            Make beautiful transitions
                                                                            <small class="pull-right">80%</small>
                                                                        </h3>
                                                                        <div class="progress xs">
                                                                            <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                                                <span class="sr-only">80% Complete</span>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </li> end task item
                                                            </ul>
                                                        </li>
                                                        <li class="footer">
                                                            <a href="#">View all tasks</a>
                                                        </li>
                                                    </ul>
                                                </li>-->
                        <!-- User Account: style can be found in dropdown.less -->

                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>Admin <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="<?= base_url() ?>img/avatar5.png" class="img-circle" alt="User Image" />
                                    <p>
                                        Admin
                                        <small>Administrator</small>
                                    </p>
                                </li>
                                <li class="user-footer">
<!--                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>-->
                                    <div class="pull-right">
                                        <a href="<?php echo base_url(); ?>index.php/login/logout" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>