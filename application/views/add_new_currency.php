<?php include 'inc/header.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add New Currency
            </h1>
        </section>
        <section class="content">

            <div style="margin-left: 20px;">
                <form  class="form-horizontal" method="post" action="" name="add_new_currency" enctype="multipart/form-data">
                    <fieldset>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="currency_name">Currency Name</label>
                                <div class="col-md-4">
                                    <input class="form-control" name="currency_name" type="text" placeholder="Name">
                                </div>
                            </div>
                        </div><!-- ./col -->
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="news_title">Currency Code</label>
                                <div class="col-md-2">
                                    <input class="form-control" name="currency_code" type="text" placeholder="i.e USD">
                                </div>
                            </div>
                        </div><!-- ./col -->
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="news_title">Currency Symbol</label>
                                <div class="col-md-2">
                                    <input class="form-control" name="currency_symbol" type="text" placeholder="i.e $">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 control-label"></label>
                                <div class="col-md-3">
                                    <button value="add-currency-submit" name="add-currency-submit" class="btn btn-primary">Submit</button>
                                    <a class="btn btn-default" href="<?= base_url() ?>index.php/system/currency_listing">Cancel</a>
                                </div>

                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </section>
    </aside>
</div>
<script>
    document.getElementById("website_tab").className = "treeview active";
    document.getElementById("currency").className = "active";
</script>
<?php include 'inc/footer.php'; ?>