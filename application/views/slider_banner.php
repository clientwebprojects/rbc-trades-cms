<?php include 'inc/header.php'; ?>

<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->


        <section class="content">
            <a href="<?= base_url() ?>index.php/system/add_new_slider_images" class="btn btn-primary pull-right add_new_country_btn">Add Images</a>
            <h2 class="page-header">Home Slider</h2>
            <div class="row">
                <div class="col-md-12">
                    <!-- Custom Tabs -->
                    <div class="box">
                        <div class="box-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Image Description</th>
                                        <th>Image Path</th>
                                        <th style="width: 100px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($slider_images as $row) {

                                        ?>

                                        <tr>
                                            <td><a href="#"><?= $row->id; ?></a></td>
                                            <td><a href="#"><?= $row->image_description; ?></a></td>
                                            <td><a href="#"><img src="<?=$row->image_path ?>" width="100px" height="100px"/></a></td>
                                            <td class="btn-group" style="width: 100px; height:100px">
                                                <a class="btn btn-xs btn-danger" href="<?php echo base_url() ?>index.php/system/delete_slider_image/<?php echo $row->id; ?>" onclick="return confirm('Are you sure to Delete?')">Delete</a>
                                            </td>

                                        </tr>

                                        <?php
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>

                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->

                    </div><!-- /.col -->


                </div> <!-- /.row -->ٖ
            </div>
        </section>

    </aside>
</div>
<script>
    document.getElementById("banner_tab").className = "treeview active";
    document.getElementById("home_slider").className = "active";
</script>
<?php include 'inc/footer.php'; ?>

