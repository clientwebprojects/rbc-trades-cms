ِ<?php include 'inc/header.php';
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">
        <section class="content">
<!--            <a data-toggle="modal" class="btn btn-primary pull-right add_new_country_btn">Add Country</a>-->
            <h2 class="page-header">Country List</h2>
            <div class="row">
                <div class="col-md-12">
                    <!-- Custom Tabs -->
                      <div class="box">

                                    <div class="box">

                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Country Name</th>
                                                <th style="width: 100px">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($countries as $country) {
                                                ?>

                                                <tr>
                                                    <td><?php echo $country->country_id; ?></td>
                                                    <td><?php echo ucfirst($country->country_name); ?></td>
                                                    <td class="btn-group" style="width: 100px">
                                                        <a class="btn btn-xs btn-primary update_country" data-toggle="modal" data-id="<?= $country->country_id?>" data-todo='{"country_name":"<?= $country->country_name?>"}'>Edit</a>
                                                        <a class="btn btn-xs btn-danger" href="<?php echo base_url()?>index.php/system/delete_country/<?php echo $country->country_id?>" onclick="return confirm('Are you sure to Delete?')">Delete</a>
                                                    </td>

                                                </tr>

                                                <?php
                                            }
                                            ?>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>

                                            </tr>
                                        </tfoot>
                                       </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                </div><!-- /.col -->


            </div> <!-- /.row -->ٖ
            </div>
        </section>

    </aside>

</div>

<script>
    document.getElementById("website_tab").className = "treeview active";
    document.getElementById("country").className = "active";
</script>
<?php
include 'inc/footer.php';
include 'inc/modals.php';
?>