<?php
include 'inc/header.php';
$var = json_encode($all_category);
$varmain = json_encode($category);
// echo '<pre>';
// print_r($category);
// exit();
?>
<script>
    var sub_category_object = <?= $var; ?>;
    var category_object = <?= $varmain; ?>;
    var count = 0;
    var maxcount = 1;
    var check_multiple = [];
    var counter = 0;
    var catId;
    var catName;
    //console.log(sub_category_object);

</script>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->

        <section class="content">


            <form class="form-horizontal" method="POST" name="add_category_form" action="">
                <fieldset>

                    <!-- Form Name -->
                    <legend>Add Category</legend>
                    <input type="hidden" id="newparentid" name="parent_id">
                    <!-- Select Multiple -->
                    <div class="form-group" id="catSelect">
                        <label class="col-md-2 control-label">Select Category</label>
                        <div class="col-md-4">
                            <select id="0" name="select" class="form-control" size="10" onchange="catFunction(this.value, this.id)" >
                                <option value="0" name="main" style="font-size: 17px;color: #000">Main Category</option>
                                <?php foreach ($category as $values) { ?>
                                    <option value="<?php echo $values->category_id; ?>" name="<?php echo $values->category_name; ?>"><?php echo $values->category_name; ?></option>

                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" >
                        <label class="col-md-2 control-label" for="category_name_textfield">Category Name</label>
                        <div class="col-md-4">
                            <input id="category_name_textfield" name="category_name_textfield" type="text" placeholder="" class="form-control input-md">

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="add_category_form"></label>
                        <div class="col-md-4">
                            <button type="submit" name="add_category_form" value="add_category_form" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </fieldset>
            </form>
        </section>
    </aside>

</div>
<script src="<?= base_url() ?>js/category-listing.js" type="text/javascript"></script>
<script>
                                function catFunction(panelid, id) {

                                    hasSub = 0;
                                    catId = panelid;
                                    document.getElementById('newparentid').setAttribute('value', catId);
//        console.log(catId);
                                    if (id === '0') {
                                        $.each(category_object, function(i, v) {
                                            if (parseInt(v.category_id) === parseInt(panelid)) {

                                                catName = v.category_name;
                                            }

                                        });

                                    }
                                    else {
                                        $.each(sub_category_object, function(i, v) {


                                            if (parseInt(v.category_id) === parseInt(panelid)) {

                                                catName = v.category_name;
                                            }

                                        });
                                    }
                                    if (count > id) {
                                        i = count;
                                        while (i > id) {
                                            $('#main' + i).remove();
                                            i--;
                                        }
                                        count = id;

                                    }
                                    count++;

                                    $.each(sub_category_object, function(i, v) {

                                        if (parseInt(v.parent_id) === parseInt(panelid)) {
                                            hasSub++;
                                        }

                                    });


                                    var start_html =
                                            '<div class="col-md-4" id="main' + count + '"> ' +
                                            '<select id="' + count + '" name="select' + count + '" class="form-control" size="12" onchange="catFunction(this.value,this.id)" >';
                                    var end_html =
                                            '</select>' +
                                            '</div>';
                                    console.log(hasSub);
                                    if (hasSub > 0) {
                                        $('#catSelect').append(start_html);

                                        $.each(sub_category_object, function(i, v) {

                                            if (parseInt(v.parent_id) === parseInt(panelid)) {

                                                var subCategory_html =
                                                        '<option value="' + v.category_id + '">' +
                                                        v.category_name +
                                                        '</option>';

                                                $('#' + count).append(subCategory_html);

                                            }

                                        });
                                        $('#catSelect').append(end_html);
                                    }
                                    $("#loader").hide();
                                }
</script>
<script>
    document.getElementById("website_tab").className = "treeview active";
    document.getElementById("category_management").className = "active";
</script>
<?php include 'inc/footer.php'; ?>