<?php include 'inc/header.php'; ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.js"></script>
<script type="text/javascript">
    var abc = 0;
    var image_counter=0;
    $(document).ready(function () {
        $('body').on('change', '#slide_images', function () {
            if (this.files && this.files[0] && image_counter < 4) {
                abc += 1; //increementing global variable by 1

                var z = abc - 1;
                var x = $(this).parent().find('#previewimg' + z).remove();
                $(this).before("<div id='image" + abc + "' class='img-div'><img id='previewimg" + abc + "' src=''/></div>");

                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
                $(this).before($("<div/>", {id: 'filediv'}).fadeIn('slow').append(
                        $("<input/>", {name: 'slider_image[]', type: 'file', id: 'slide_images', class: 'btn btn-default btn-sm attach_file_btn custom'})
                        ));
                image_counter++;
                $(this).hide();
                $("#image" + abc).append($("<img/>", {id: 'imgx', src: 'http://localhost/rbctrades/images/x.png', alt: 'delete'}).click(function () {
                    $(this).parent().remove();
                    image_counter--;
                }));
            }
            else {
                alert("You can upload maximum 4 images");
            }
        });
    });
    function imageIsLoaded(e) {
        $('#previewimg' + abc).attr('src', e.target.result);
    }
</script>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Slider Images Management
            </h1>
        </section>
        <section class="content">

            <div style="margin-left: 20px;">
                <form id="seller-add-new-images" class="form-horizontal" method="post" action="" name="add_new_slider_images" enctype="multipart/form-data">

                    <fieldset>


                        <span class="small-desc"></span>
                        <div class="row">

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="Image_description">Image Description</label>
                                <div class="col-md-6">
                                    <textarea id="Image_description" name="Image_description" type="text"  rows="5" cols="20" class="form-control"></textarea>


                                </div>
                            </div>
                        </div><!-- ./col -->
                        <span class="small-desc"></span>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Slider Image</label>
                                <div class="col-md-4">
                                    <input class="btn btn-default btn-sm" id="slide_images" type="file" name="slider_image[]">

                                </div>

                            </div>
                            <!-- /.row -->
                        </div>
                        <span class="small-desc"></span>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <button  id="add-product" style=" margin-left: 190px;" value="add-slider-submit" name="add-slider-submit" class="btn button_blue">Submit</button>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </section>
    </aside>
</div>
<script>
    document.getElementById("banner_tab").className = "treeview active";
    document.getElementById("home_slider").className = "active";
</script>
<?php include 'inc/footer.php'; ?>

