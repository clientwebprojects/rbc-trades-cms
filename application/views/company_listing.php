ِ<?php include 'inc/header.php';
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->


        <section class="content">

            <h2 class="page-header">User Management</h2>
            <div class="row">
                <div class="col-md-12">
                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab" style="font-weight: 600">Verified Companies</a></li>
                            <li><a href="#tab_2" data-toggle="tab" style="font-weight: 600">Unverified Companies</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">

                                <div class="box">

                                    <div class="box-body table-responsive">
                                        <table id="example1" class="table table-bordered table-striped">
                                            <thead>

                                                <tr>
                                                    <th>ID</th>
                                                    <th>Company Name</th>
                                                    <th>Company Owner</th>
                                                    <th>Membership</th>
                                                    <th style="width: 100px">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $count=0;
                                                foreach ($verified_companies as $verified) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo ++$count; ?></td>
                                                        <td><?= $verified->company_name; ?></td>
                                                        <td style='font-style: italic;'><?= $verified->legal_owner; ?></td>
                                                        <td style='font-style: italic;'></td>
                                                                <td class="btn-group" style="width: 100px">
                                                                    <a class="btn btn-xs btn-primary update_country" href="">View</a>
                                                            <a class="btn btn-xs btn-danger" href="<?php echo base_url() ?>index.php/users/do_bann_user/<?php echo $active->user_id ?>" onclick="return confirm('Are you sure you want to block this company?')">Block</a>
                                                    </td>

                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div><!-- /.box-body -->
                                </div><!-- /.box -->

                            </div><!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">
                                <div class="box">

                                    <div class="box-body table-responsive">
                                        <table id="table2" class="table table-bordered table-striped">
                                            <thead>

                                                <tr>
                                                    <th>ID</th>
                                                    <th>User Name</th>
                                                    <th>Member Type</th>
                                                    <th>Company Name</th>
                                                    <th>Company Status</th>
                                                    <th style="width: 100px">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
<?php
//print_r($countries);
foreach (unverified_companies as $banned) {
    //print_r($values);
    ?>
                                                    <tr>
                                                        <td><a href="#"><?php echo $banned->user_id; ?></a></td>
                                                        <td><a href="#"><?= $banned->user_name; ?></a></td>
                                                        <td style='font-style: italic;'><?= $banned->member_type; ?></td>
                                                        <td style='font-style: italic;'><?= $banned->company_name; ?></td>
                                                        <td style='font-style: italic;'><?php
                                                if ($banned->status == 0) {
                                                    echo"Unverfied";
                                                } else {
                                                    echo"Verfied";
                                                }
    ?></td>



                                                        <td style="width: 100px">

                                                            <a href="<?php echo base_url() ?>index.php/users/un_bann_user/<?php echo $banned->user_id ?>" class="btn btn-sm btn-danger" style="float:right;" ><span > Un Block </span></a>
                                                        </td>
                                                    </tr>
    <?php
}
?>
                                            </tbody>
                                        </table>

                                    </div><!-- /.box-body -->
                                </div><!-- /.box -->
                            </div><!-- /.tab-pane -->
                        </div><!-- /.tab-content -->
                    </div><!-- nav-tabs-custom -->
                </div><!-- /.col -->


            </div> <!-- /.row -->ٖ
        </section>

    </aside>

</div>
<script>
    document.getElementById("company_tab").className = "treeview active";
    document.getElementById("company_list").className = "active";
</script>
<?php
include 'inc/footer.php';
include 'inc/modals.php';
?>