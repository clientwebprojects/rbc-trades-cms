ِ<?php include 'inc/header.php';
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">
        <section class="content">
            <a data-toggle="modal" class="btn btn-primary pull-right add_new_package_btn">Add New Package</a>
            <h2 class="page-header">Membership Packages</h2>
            <div class="row">
                <div class="col-md-12">
                    <!-- Custom Tabs -->
                    <div class="box">

                        <div class="box">

                            <div class="box-body table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 50px;">ID</th>
                                            <th>Package Fee</th>
                                            <th>Duration</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($membership_package as $value) {
                                            ?>

                                            <tr>
                                                    <td><?php echo $value->id; ?></td>
                                                    <td><?php echo $value->currency_code." ".$value->currency_symbol."".number_format($value->package_fee); ?></td>
                                                    <td><?php echo $value->package_duration; ?></td>
                                                    <td class="btn-group" style="width: 100px">
                                                        <a class="btn btn-xs btn-primary update_membership_package" data-toggle="modal" data-id="<?= $value->id?>" data-todo='{"package_fee":"<?= $value->package_fee?>","currency_id":"<?= $value->currency_id?>","package_duration":"<?= $value->package_duration?>"}'>Edit</a>
                                                        <a class="btn btn-xs btn-danger" href="<?php echo base_url()?>index.php/system/delete_department/<?php echo $value->id?>" onclick="return confirm('Are you sure to Delete?')">Delete</a>
                                                    </td>

                                                </tr>

                                            <?php
                                        }
                                        ?>
                                    </tbody>

                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.col -->


                </div> <!-- /.row -->ٖ
            </div>
        </section>

    </aside>

</div>
<script>
    document.getElementById("membership_tab").className = "treeview active";
    document.getElementById("membership_package_listing").className = "active";
</script>
<?php
include 'inc/footer.php';
include 'inc/modals.php';
?>