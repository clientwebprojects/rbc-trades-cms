<?php
include 'inc/header.php';
$var = json_encode($all_category);
$varmain = json_encode($category);
// echo '<pre>';
// print_r($category);
// exit();
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.js"></script>
<script>
    var sub_category_object = <?= $var; ?>;
    var category_object = <?= $varmain; ?>;
    var count=0;
    var catId;
    var catName;
    //console.log(sub_category_object);
</script>
<script> var image_counter = 0;</script>
<script type="text/javascript">

    $(document).ready(function () {
        $("form#seller-add-new-product").on('submit', (function (e) {
            e.preventDefault();
            $.ajax({
                url: "<?= base_url() ?>index.php/system/add_new_product/>",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function () {
                    $('html, body').animate({scrollTop: 0}, 0);
                    $("#successmessage").html("<div class='alert alert-success alert-dismissible' role='alert' ><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Congrats!</strong>Product Added Successfully.</div>");

                },
                error: function () {
                }
            });
        }));

    });
</script>
<script type="text/javascript">

    $(document).ready(function () {

        $("#seller-add-new-product").on('submit', (function (e) {
            $("#upload").hide();
        }));
    });
</script>
<script type="text/javascript">
    var abc = 0;
    $(document).ready(function () {
        $('body').on('change', '#file', function () {
            if (this.files && this.files[0] && image_counter < 4) {
                abc += 1; //increementing global variable by 1

                var z = abc - 1;
                var x = $(this).parent().find('#previewimg' + z).remove();
                $(this).before("<div id='image" + abc + "' class='img-div'><img id='previewimg" + abc + "' src=''/></div>");

                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
                $(this).before($("<div/>", {id: 'filediv'}).fadeIn('slow').append(
                        $("<input/>", {name: 'product_image[]', type: 'file', id: 'file', class: 'btn btn-default btn-sm attach_file_btn custom'})
                        ));
                image_counter++;
                $(this).hide();
                $("#image" + abc).append($("<img/>", {id: 'imgx', src: 'http://localhost/rbctrades/images/x.png', alt: 'delete'}).click(function () {
                    $(this).parent().remove();
                    image_counter--;
                }));
            }
            else {
                alert("You can upload maximum 4 images");
            }
        });
    });
    function imageIsLoaded(e) {
        $('#previewimg' + abc).attr('src', e.target.result);
    }
</script>

<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->


        <section>
            <div style="margin-left: 20px;">
           <form id="seller-add-new-product" class="form-horizontal" method="post" action="" name="add_new_product" enctype="multipart/form-data">

                                <fieldset>
                                    <legend>Add a New Product</legend>
                                    <div id="successmessage"></div>

                                    <h4 class="heading">Basic Information</h4>

                                    <input type="hidden" id="newparentid" name="product_category">
                                    <div style="background-color: #f5f5f5;padding: 20px 0 5px;border: 1px solid #bebebe;border-radius: 3px;margin-bottom: 14px;">
                                        <div class="form-group" id="catSelect">
                                            <label class="col-md-2 control-label">Select Category</label>
                                            <div class="col-md-4">
                                                <select id="0" name="select" class="form-control" size="10" onchange="catFunction(this.value, this.id)" >
                                                    <?php foreach ($category as $values) { ?>
                                                        <option value="<?php echo $values->category_id; ?>" name="<?php echo $values->category_name; ?>"><?php echo $values->category_name; ?></option>

<?php } ?>
                                                </select>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="clearfix"></div>


                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="product_name">Product Name</label>
                                        <div class="col-md-6">
                                            <input id="product_name" name="product_name" type="text" placeholder="" class="form-control input-md"   >

                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="product_keyword">Product Keyword</label>
                                        <div class="col-md-6">
                                            <input id="product_keyword" name="product_keyword" type="text" placeholder="Enter one key only -e.g. Road bike" class="form-control input-md"   >

                                        </div>
                                    </div>

                                    <!-- File Button -->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Product Image</label>
                                        <div class="col-md-4">
                                            <input class="btn btn-default btn-sm" id="file" type="file" name="product_image[]">
                                            <span class="small-desc">Note: First image is the product's main image.</span>
                                        </div>

                                    </div>

                                    <div class="dash-line"></div>
                                    <h4 class="heading">Product Details </h4>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="product_model">Model Number</label>
                                        <div class="col-md-6">
                                            <input id="model_number" name="product_model" type="text" placeholder="" class="form-control input-md">

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="product_code">Product Code</label>
                                        <div class="col-md-6">
                                            <input id="product_code" name="product_code" type="text" placeholder="" class="form-control input-md">

                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="brand_id">Brand Id</label>
                                        <div class="col-md-6">
                                            <input id="brand_id" name="brand_id" type="text" placeholder="" class="form-control input-md">

                                        </div>
                                    </div>

                                    <!-- Select Basic -->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="place_of_origin">Place of Origin</label>
                                        <div class="col-md-6">
                                            <select id="place_of_origin" name="product_origin" class="form-control">
                                                <option value="">--Please Select</option>
                                                <?php foreach ($countries as $country) { ?>
                                                    <option value="<?= $country->country_id; ?>"><?= $country->country_name; ?></option>
<?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="uom">UOM</label>
                                        <div class="col-md-4">
                                            <select id="uom" name="uom_id" class="form-control">
                                                <option value="">--Please Select</option>
                                                <?php foreach ($uom as $unit_of_m) { ?>
                                                    <option value="<?= $unit_of_m->uom_id; ?>"><?= $unit_of_m->uom_name; ?></option>
<?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Password input-->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="product_type">Type</label>
                                        <div class="col-md-4">
                                            <input id="product_type" name="product_type" type="text" placeholder="" class="form-control input-md">

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="use">Use</label>
                                        <div class="col-md-6">
                                            <select id="place_of_origin" name="product_use" class="form-control">
                                                <option value="">--Please Select</option>

                                                <option value="1">Other</option>

                                                <option value="2">Bird</option>

                                                <option value="3">Chicken</option>

                                                <option value="4">Rabbit</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="product_description">More Details</label>
                                        <div class="col-md-6">
                                            <textarea id="product_description" name="product_description" type="text" rows="5" class="form-control input-md"></textarea>


                                        </div>
                                    </div>
                                    <div class="dash-line"></div>
                                    <h4 class="heading">Trade Information </h4>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="minimum_order_quantity">Minimum Order Quantity</label>
                                        <div class="col-md-6">
                                            <input id="minimum_order_quantity" name="product_minimun_order" type="text" placeholder="" class="form-control input-md">

                                        </div>
                                    </div>

                                    <!-- Search input-->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="fob_price">FOB Price</label>
                                        <div class="col-md-4">
                                            <input id="fob_price" name="product_fob_price" type="text" placeholder="" class="form-control input-md">

                                        </div>
                                    </div>

                                    <!-- Password input-->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="port">Port</label>
                                        <div class="col-md-4">
                                            <input id="port" name="product_port" type="text" placeholder="" class="form-control input-md">

                                        </div>
                                    </div>

                                    <!-- Multiple Checkboxes (inline) -->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="payment_terms">Payment Terms</label>
                                        <div class="col-md-10" >

                                            <?php
                                            foreach ($ptm as $payment_type_val) {
                                                ?>
                                                <label class="checkbox-inline" for="">
                                                    <input type="checkbox" name="product_payment_type[]" value="<?= $payment_type_val->p_term_id; ?>"   style=" width: 30px">
                                                <?= $payment_type_val->p_term_description; ?>
                                                </label>

                                                <?php
                                            }
                                            ?>


                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="supply_ability">Supply Ability</label>
                                        <div class="col-md-6">
                                            <input id="supply_ability" name="product_supply_ability" type="text" placeholder="Quantity in pieces" class="form-control input-md">

                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="delievery_time">Delivery Time</label>
                                        <div class="col-md-6">
                                            <input id="delievery_time" name="product_delivery_time" type="text" placeholder="In Days" class="form-control input-md">

                                        </div>
                                    </div>

                                    <!-- Textarea -->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="pakagin_details">Packaging Details</label>
                                        <div class="col-md-6">
                                            <textarea class="form-control" id="pakagin_details" rows="5" name="packaging_detail"></textarea>
                                        </div>
                                    </div>
                                    <div class="dash-line"></div>
                                    <h4 class="heading">Detailed Description</h4>
                                    <!-- Textarea -->
                                    <div class="form-group">

                                        <div class="col-md-12" id="eButtons">
                                            <textarea cols="100" id="editor1" name="editor1" rows="10"></textarea>

                                            <script>
                                                // Replace the <textarea id="editor1"> with an CKEditor instance.
                                                CKEDITOR.replace('editor1', {
                                                    on: {
                                                        focus: onFocus,
                                                        blur: onBlur,
                                                        // Check for availability of corresponding plugins.
                                                        pluginsLoaded: function (evt) {
                                                            var doc = CKEDITOR.document, ed = evt.editor;
                                                            if (!ed.getCommand('bold'))
                                                                doc.getById('exec-bold').hide();
                                                            if (!ed.getCommand('link'))
                                                                doc.getById('exec-link').hide();
                                                        }
                                                    }
                                                });
                                            </script>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <button  id="add-product" value="add-product-submit" name="add-product-submit" class="btn button_blue">Submit</button>
                                        </div>
                                    </div>
                                </fieldset>
                                <!-- Button -->
                            </form>
            </div>
        </section>

    </aside>

</div>
<script src="<?= base_url() ?>js/category-listing.js" type="text/javascript"></script>
<script>
                                function catFunction(panelid, id) {

                                    hasSub = 0;
                                    catId = panelid;
                                    document.getElementById('newparentid').setAttribute('value', catId);
//        console.log(catId);
                                    if (id === '0') {
                                        $.each(category_object, function(i, v) {
                                            if (parseInt(v.category_id) === parseInt(panelid)) {

                                                catName = v.category_name;
                                            }

                                        });

                                    }
                                    else {
                                        $.each(sub_category_object, function(i, v) {


                                            if (parseInt(v.category_id) === parseInt(panelid)) {

                                                catName = v.category_name;
                                            }

                                        });
                                    }
                                    if (count > id) {
                                        i = count;
                                        while (i > id) {
                                            $('#main' + i).remove();
                                            i--;
                                        }
                                        count = id;

                                    }
                                    count++;

                                    $.each(sub_category_object, function(i, v) {

                                        if (parseInt(v.parent_id) === parseInt(panelid)) {
                                            hasSub++;
                                        }

                                    });


                                    var start_html =
                                            '<div class="col-md-4" id="main' + count + '"> ' +
                                            '<select id="' + count + '" name="select' + count + '" class="form-control" size="10" onchange="catFunction(this.value,this.id)" >';
                                    var end_html =
                                            '</select>' +
                                            '</div>';
                                    console.log(hasSub);
                                    if (hasSub > 0) {
                                        $('#catSelect').append(start_html);

                                        $.each(sub_category_object, function(i, v) {

                                            if (parseInt(v.parent_id) === parseInt(panelid)) {

                                                var subCategory_html =
                                                        '<option value="' + v.category_id + '">' +
                                                        v.category_name +
                                                        '</option>';

                                                $('#' + count).append(subCategory_html);

                                            }

                                        });
                                        $('#catSelect').append(end_html);
                                    }
                                    $("#loader").hide();
                                }
</script>
<script>
    document.getElementById("product_tab").className = "treeview active";
    document.getElementById("company_product").className = "active";
</script>
<?php
include 'inc/footer.php';
?>