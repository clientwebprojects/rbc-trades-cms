ِ<?php
include 'inc/header.php';
//echo "<pre>";
//print_r($membership);exit;
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">
        <section class="content">
            <h2 class="page-header">RBC Membership Types</h2>
            <div class="row">
                <div class="col-md-12">
                    <!-- Custom Tabs -->
                    <div class="box">

                        <div class="box">

                            <div class="box-body table-responsive">
                                <table class="table table-bordered table-striped" style="font-size: 12px;text-align: center;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">ID</th>
                                            <th style="width: 100px;">Membership</th>
                                            <th style="text-align: center;width: 50px;">Product Approval</th>
                                            <th style="text-align: center;width: 50px;">Product Ranking</th>
                                            <th style="text-align: center;width: 50px;">Product Limit</th>
                                            <th style="text-align: center;width: 50px;">Quote Limit</th>
                                            <th style="text-align: center;width: 50px;">RFQ Limit</th>
                                            <th style="text-align: center;width: 50px;">Customized Website</th>
                                            <th style="text-align: center;width: 50px;">Chat Messenger</th>
                                            <th style="width: 200px;">Package Detail</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($membership as $value) {
                                            ?>

                                            <tr>
                                                <td><?php echo $value->membership_id; ?></td>
                                                <td style="text-align: left;"><?php echo $value->membership_name; ?></td>
                                                <td>
                                                    <?php if ($value->product_approval == 0) { ?>
                                                        <i class="glyphicon glyphicon-remove"></i>
                                                    <?php } elseif ($value->product_approval == 1) { ?>
                                                        <i class="glyphicon glyphicon-ok"></i>
                                                    <?php } elseif ($value->product_approval == 2) { ?>
                                                        Approval on Priority
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <?php if ($value->product_listing_priority == 1) { ?>
                                                        Lowest priority
                                                    <?php } elseif ($value->product_listing_priority == 2) { ?>
                                                        2nd highest priority
                                                    <?php } elseif ($value->product_listing_priority == 3) { ?>
                                                        Highest priority
                                                    <?php } ?>
                                                </td>
                                                <td><?php echo $value->product_limit; ?></td>
                                                <td><?php echo $value->build_quotation_limit; ?></td>
                                                <td><?php echo $value->get_quotation_limit; ?></td>
                                                <td>
                                                    <?php if ($value->mini_site == 0) { ?>
                                                        <i class="glyphicon glyphicon-remove"></i>
                                                    <?php } elseif ($value->mini_site == 1) { ?>
                                                        <i class="glyphicon glyphicon-ok"></i>
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <?php if ($value->chat_messenger == 0) { ?>
                                                        <i class="glyphicon glyphicon-remove"></i>
                                                    <?php } elseif ($value->chat_messenger == 1) { ?>
                                                        <i class="glyphicon glyphicon-ok"></i>
                                                    <?php } ?>
                                                </td>
                                                <td style="text-align: left;">
                                                    <?php
                                                    if ($value->package_id == 0) {
                                                        echo "Fee";
                                                    } else {
                                                        echo $value->currency_code . " " . $value->currency_symbol . "" . number_format($value->package_fee) . " " . $value->package_duration;
                                                    }
                                                        ?>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <a class="btn btn-xs btn-primary update_country" href="<?php echo base_url() ?>index.php/system/membership_edit/<?php echo $value->membership_id ?>">Edit</a>
                                                    </td>

                                                </tr>

                                                <?php
                                            }
                                            ?>
                                        </tbody>

                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!-- /.col -->


                    </div> <!-- /.row -->ٖ
                </div>
            </section>

        </aside>

    </div>
    <script>
        document.getElementById("membership_tab").className = "treeview active";
        document.getElementById("membership_listing").className = "active";
    </script>
    <?php
    include 'inc/footer.php';
    ?>