ِ<?php include 'inc/header.php';
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->


        <section class="content">
            <a data-toggle="modal" class="btn btn-primary pull-right add_export_btn">Add Export Percentage</a>
            <h2 class="page-header">Export Percentage Management</h2>
            <div class="row">
                <div class="col-md-12">
                    <!-- Custom Tabs -->
                      <div class="box">

                                    <div class="box">

                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Description</th>
                                                <th style="width: 100px">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($all_export as $value) {
                                                ?>

                                                <tr>
                                                    <td><?php echo $value->id; ?></td>
                                                    <td><?php echo ucfirst($value->description); ?></td>
                                                    <td class="btn-group" style="width: 100px">
                                                        <a class="btn btn-xs btn-primary update_export_percentage" data-toggle="modal" data-id="<?= $value->id?>" data-todo='{"description":"<?= $value->description?>"}'>Edit</a>
                                                        <a class="btn btn-xs btn-danger" href="<?php echo base_url()?>index.php/system/delete_export_percentage/<?php echo $value->id?>" onclick="return confirm('Are you sure to Delete?')">Delete</a>
                                                    </td>

                                                </tr>

                                                <?php
                                            }
                                            ?>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>

                                            </tr>
                                        </tfoot>
                                       </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                </div><!-- /.col -->


            </div> <!-- /.row -->ٖ
            </div>
        </section>

    </aside>

</div>
<script>
    document.getElementById("website_tab").className = "treeview active";
    document.getElementById("export_percent").className = "active";
</script>
<?php
include 'inc/footer.php';
include 'inc/modals.php';
?>