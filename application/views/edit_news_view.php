<?php include 'inc/header.php'; ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.js"></script>

<script type="text/javascript">
    var abc = 0;
    $(document).ready(function () {
        $('body').on('change', '#news_image', function () {
            if (this.files && this.files[0] ) {
                abc += 1; //increementing global variable by 1

                var z = abc - 1;
                var x = $(this).parent().find('#previewimg' + z).remove();
                $(this).before("<div id='image" + abc + "' class='img-div'><img id='previewimg" + abc + "' src=''/></div>");

                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);

                $(this).hide();
                $("#image" + abc).append($("<img/>", {id: 'imgx', src: 'http://localhost/rbctrades/images/x.png', alt: 'delete'}).click(function () {
                    $(this).parent().remove();

                    $('#news_image').show();

                }));
            }
            else {
                alert("You can upload maximum 4 images");
            }
        });
    });
    function imageIsLoaded(e) {
        $('#previewimg' + abc).attr('src', e.target.result);
    }
</script>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add New News
            </h1>
        </section>
        <section class="content">

            <div style="margin-left: 20px;">
                <form class="form-horizontal" method="post" action="" name="edit_new_news" enctype="multipart/form-data">

                    <fieldset>


                        <div class="row">

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="news_title">News Title</label>
                                <div class="col-md-6">
                                    <input class="form-control" name="news_title" type="text" value="<?=$edit_news->news_title;?>" >


                                </div>
                            </div>
                        </div><!-- ./col -->

                        <div class="row">

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="news_description">News Description</label>
                                <div class="col-md-6">
                                    <textarea id="news_description" name="news_description" type="text" rows="5" class="form-control input-md"><?=$edit_news->news_description;?></textarea>


                                </div>
                            </div>
                        </div><!-- ./col -->

                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 control-label">News Image</label>
                                <div class="col-md-4">
                                    <input class="btn btn-default btn-sm" id="news_image" type="file" name="news_image">
                                    <span class="small-desc"></span>

                                </div>

                            </div>
                        </div>

                        <span class="small-desc"></span>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Already Uploaded</label>
                                <div class="col-md-4">
                                    <img src="<?php echo $edit_news->news_image; ?>" width="100px" height="100px"/>
                                    <span class="small-desc"></span>

                                </div>

                            </div>
                        </div>

                        <div class="row">

                            <div class="form-group">
                                <div class="col-md-3">
                                    <button  id="add-news" style=" margin-left: 200px;" value="edit-news-submit" name="edit-news-submit" class="btn button_blue">Update</button>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                </form>
            </div>
        </section>
    </aside>
</div>


<?php include 'inc/footer.php'; ?>

