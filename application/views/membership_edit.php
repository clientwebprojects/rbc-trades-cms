<?php include 'inc/header.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Membership Detail
            </h1>
        </section>
        <hr/>
        <section class="content">
            <div style="margin-left: 20px;">
                <form class="form-horizontal" method="post" action=""  enctype="multipart/form-data">
                    <input type="hidden" name="membership_id" value="<?=$membership->membership_id; ?>"/>
                    <fieldset>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="membership_name">Membership Name</label>
                                <div class="col-md-4">
                                    <input class="form-control" name="membership_name" value="<?=$membership->membership_name; ?>" type="text" placeholder="Name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Membership Package</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="package_id">
                                        <option value="0" <?php echo ($membership->package_id==0)?"selected":""; ?>>Free</option>
                                        <?php foreach ($membership_package as $value){ ?>
                                        <option value="<?=$value->id?>" <?php echo ($membership->package_id==$value->id)?"selected":""; ?>><?php echo $value->currency_code . " " . $value->currency_symbol . "" . number_format($value->package_fee) . "  " . $value->package_duration; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="product_approval">Product Approval</label>
                                <div class="col-md-3">
                                    <select class="form-control" name="product_approval">
                                        <option value="0" <?php echo ($membership->product_approval==0)?"selected":""; ?>>No</option>
                                        <option value="1" <?php echo ($membership->product_approval==1)?"selected":""; ?>>Yes</option>
                                        <option value="2" <?php echo ($membership->product_approval==2)?"selected":""; ?>>Approval on Priority</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="product_listing_priority">Product Ranking</label>
                                <div class="col-md-3">
                                    <select class="form-control" name="product_listing_priority">
                                        <option value="1" <?php echo ($membership->product_listing_priority==1)?"selected":""; ?>>Lowest Priority</option>
                                        <option value="2" <?php echo ($membership->product_listing_priority==2)?"selected":""; ?>>2nd Highest Priority</option>
                                        <option value="3" <?php echo ($membership->product_listing_priority==3)?"selected":""; ?>>Highest Priority</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="product_limit">Product Limit</label>
                                <div class="col-md-3">
                                    <input class="form-control" name="product_limit" type="text" value="<?=$membership->product_limit; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="build_quotation_limit">Quote to Buying Request Limit</label>
                                <div class="col-md-3">
                                    <input class="form-control" name="build_quotation_limit" type="text" value="<?=$membership->build_quotation_limit; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="get_quotation_limit">Request for Quotation Limit</label>
                                <div class="col-md-3">
                                    <input class="form-control" name="get_quotation_limit" type="text" value="<?=$membership->get_quotation_limit; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="mini_site">Customized Website</label>
                                <div class="col-md-3">
                                    <select class="form-control" name="mini_site">
                                        <option value="0" <?php echo ($membership->mini_site==0)?"selected":""; ?>>No</option>
                                        <option value="1" <?php echo ($membership->mini_site==1)?"selected":""; ?>>Yes</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="chat_messenger">Chat Messenger</label>
                                <div class="col-md-3">
                                    <select class="form-control" name="chat_messenger">
                                        <option value="0" <?php echo ($membership->chat_messenger==0)?"selected":""; ?>>No</option>
                                        <option value="1" <?php echo ($membership->chat_messenger==1)?"selected":""; ?>>Yes</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <span class="small-desc"></span>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-3">
                                    <button value="membership_edit" name="membership_edit" class="btn btn-primary">Update</button>
                                    <a class="btn btn-default" href="<?= base_url() ?>index.php/system/membership">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </section>
    </aside>
</div>
<script>
    document.getElementById("website_tab").className = "treeview active";
    document.getElementById("currency").className = "active";
</script>
<?php include 'inc/footer.php'; ?>