<?php include 'inc/header.php'; ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.js"></script>
<script type="text/javascript">
    var cat_id = <?=$edit_banners->location_id ?>

    $(document).ready(function () {


            if (cat_id == '1')
            {
                $('.cat_banner').show();

            }
            else

            {
                $(".cat_banner").hide();

            }


    });
</script>
<script type="text/javascript">
    var abc = 0;
    $(document).ready(function () {
        $('body').on('change', '#edit_image', function () {
            if (this.files && this.files[0]) {
                abc += 1; //increementing global variable by 1

                var z = abc - 1;
                var x = $(this).parent().find('#previewimg' + z).remove();
                $(this).before("<div id='image" + abc + "' class='img-div'><img id='previewimg" + abc + "' src=''/></div>");

                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);

                $(this).hide();
                $("#image" + abc).append($("<img/>", {id: 'imgx', src: 'http://localhost/rbctrades/images/x.png', alt: 'delete'}).click(function () {
                    $(this).parent().remove();
                    $('#edit_image').show();
                }));
            }
            else {
                alert("You can upload maximum 4 images");
            }
        });
    });
    function imageIsLoaded(e) {
        $('#previewimg' + abc).attr('src', e.target.result);
    }
</script>

<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Banner Images Management
            </h1>
        </section>
        <section class="content">

            <div style="margin-left: 20px;">
                <form id="seller-add-new-images" class="form-horizontal" method="post" action="" name="add_new_slider_images" enctype="multipart/form-data">

                    <fieldset>

                        <div class="row">

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="cat_banner_description">Banner Description</label>
                                <div class="col-md-6">
                                    <textarea id="product_description" value="<?=$edit_banners->banner_description; ?>" name="banner_description" type="text" rows="5" class="form-control input-md"><?=$edit_banners->banner_description; ?></textarea>


                                </div>
                            </div>
                        </div><!-- ./col -->

                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Category Banner Image</label>
                                <div class="col-md-4">
                                    <input class="btn btn-default btn-sm" id="edit_image" type="file" name="banner_image">
                                    <span class="small-desc"></span>
                                </div>

                            </div>
                        </div>
                        <div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Already Uploaded Image</label>
                                <div class="col-md-3">
                            <img src="<?=$edit_banners->image_path; ?>" width="100px" height="100px"/>
                                </div>
                        </div>
                        </div>
                        <span class="small-desc"></span>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="banner_location">Banner Location</label>
                                <div class="col-md-6">
                                    <select id="all_banner_location"  name="location_id" class="form-control">
                                        <option value="0">--Please Select</option>
                                        <?php foreach ($banner_location as $location_list) {
                                            ?>
                                            <option value="<?= $location_list->id; ?>"<?php if ($location_list->id == $edit_banners->location_id) echo 'selected="selected"'; ?>><?= $location_list->banner_location; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <span class="small-desc"></span>
                        <div class="row">
                            <div class="form-group cat_banner" style="display: none">
                                <label class="col-md-2 control-label" for="banner_location">Category Location</label>
                                <div class="col-md-6">
                                    <select id="category_banner_location" name="category_location" class="form-control" >
                                        <option value="0">--Please Select</option>
                                        <?php foreach ($category as $category_list) { ?>
                                            <option value="<?= $category_list->category_id; ?>" <?php if ($category_list->category_id == $edit_banners->category_location ) echo 'selected="selected"'; ?>><?= $category_list->category_name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <span class="small-desc"></span>
                            <div class="form-group">
                                <div class="col-md-3">
                                    <button  id="add-product" style=" margin-left: 85px;" value="update-banner-submit" name="update-banner-submit" class="btn button_blue">Update</button>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </section>
    </aside>
</div>
<script>
    document.getElementById("banner_tab").className = "treeview active";
    document.getElementById("cat_banner").className = "active";
</script>
<?php include 'inc/footer.php'; ?>

