ِ<?php include 'inc/header.php';
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->


        <section class="content">
            <a href="<?php echo base_url()?>index.php/system/add_news/" class="btn btn-primary pull-right add_new_news_btn">Add News</a>
            <h2 class="page-header">News Management</h2>
            <div class="row">
                <div class="col-md-12">
                    <!-- Custom Tabs -->
                      <div class="box">

                                    <div class="box">

                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>News Title</th>
                                                <th>News Description</th>
                                                <th>News Date</th>
                                                <th>News Image</th>

                                                <th style="width: 100px">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($news_listing as $news_val) {
                                                ?>

                                                <tr>
                                                    <td><?php echo $news_val->id; ?></td>
                                                    <td><?php echo $news_val->news_title; ?></td>
                                                    <td><?php echo $news_val->news_description; ?></td>
                                                    <td><?php echo $news_val->news_date; ?></td>
                                                    <td><img src="<?php echo $news_val->news_image; ?>" width="100px" height="100px"/></td>
                                                    <td class="btn-group" style="width: 100px; height: 100px">
                                                        <a class="btn btn-xs btn-primary update_country"  href="<?php echo base_url()?>index.php/system/edit_news/<?php echo $news_val->id?>">Edit</a>
                                                        <a class="btn btn-xs btn-danger" href="<?php echo base_url()?>index.php/system/delete_news/<?php echo $news_val->id?>" onclick="return confirm('Are you sure to Delete?')">Delete</a>
                                                    </td>

                                                </tr>

                                                <?php
                                            }
                                            ?>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>

                                            </tr>
                                        </tfoot>
                                       </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                </div><!-- /.col -->


            </div> <!-- /.row -->ٖ
            </div>
        </section>

    </aside>

</div>


<?php
include 'inc/footer.php';
include 'inc/modals.php';
?>