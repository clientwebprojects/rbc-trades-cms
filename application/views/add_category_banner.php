<?php include 'inc/header.php'; ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $('#all_banner_location').on('change', function () {
            console.log($(this).attr("value"));
            if (this.value == '1')
            {
                $('.cat_banner').show();

            }
            else

            {
                $(".cat_banner").hide();

            }
        });

    });
</script>
<script type="text/javascript">
    var abc = 0;
    $(document).ready(function () {
        $('body').on('change', '#banner_image', function () {
            if (this.files && this.files[0] ) {
                abc += 1; //increementing global variable by 1

                var z = abc - 1;
                var x = $(this).parent().find('#previewimg' + z).remove();
                $(this).before("<div id='image" + abc + "' class='img-div'><img id='previewimg" + abc + "' src=''/></div>");

                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);

                $(this).hide();
                $("#image" + abc).append($("<img/>", {id: 'imgx', src: 'http://localhost/rbctrades/images/x.png', alt: 'delete'}).click(function () {
                    $(this).parent().remove();
                    $('#banner_image').show();
                }));
            }
            else {
                alert("You can upload maximum 4 images");
            }
        });
    });
    function imageIsLoaded(e) {
        $('#previewimg' + abc).attr('src', e.target.result);
    }
</script>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add Banner Images
            </h1>
        </section>
        <section class="content">

            <div style="margin-left: 20px;">
                <form id="seller-add-new-images" class="form-horizontal" method="post" action="add_category_banner" name="add_new_slider_images" enctype="multipart/form-data">

                    <fieldset>

                        <div class="row">

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="cat_banner_description">Banner Description</label>
                                <div class="col-md-6">
                                    <textarea id="product_description" name="banner_description" type="text" rows="5" class="form-control input-md"></textarea>


                                </div>
                            </div>
                        </div><!-- ./col -->

                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Category Banner Image</label>
                                <div class="col-md-4">
                                    <input class="btn btn-default btn-sm" id="banner_image" type="file" name="banner_image">
                                    <span class="small-desc"></span>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="banner_location">Banner Location</label>
                                <div class="col-md-6">
                                    <select id="all_banner_location"  name="location_id" class="form-control">
                                        <option value="0">--Please Select</option>
                                        <?php

										foreach ($banner_location as $location_list) {
                                            ?>
                                            <option value="<?= $location_list->id; ?>"><?= $location_list->banner_location; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <span class="small-desc"></span>
                        <div class="row">
                            <div class="form-group cat_banner" style="display: none">
                                <label class="col-md-2 control-label" for="banner_location">Category Location</label>
                                <div class="col-md-6">
                                    <select id="category_banner_location" name="category_location" class="form-control" >
                                        <option value="0">--Please Select</option>
                                        <?php foreach ($categories as $category_list) { ?>
                                            <option value="<?= $category_list->category_id; ?>"><?= $category_list->category_name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <span class="small-desc"></span>
                            <div class="form-group">
                                <div class="col-md-3">
                                    <button  id="add-product" style=" margin-left: 85px;" value="add-banner-submit" name="add-banner-submit" class="btn button_blue">Submit</button>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </section>
    </aside>
</div>
<script>
    document.getElementById("banner_tab").className = "treeview active";
    document.getElementById("cat_banner").className = "active";
</script>
<?php include 'inc/footer.php'; ?>

