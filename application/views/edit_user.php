<?php
include 'inc/header.php';
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->


        <section>
            <div style="margin-left: 20px;">
            <form class="form-horizontal" method="post" action="" name="add_new_product" enctype="multipart/form-data">

                                <fieldset>
                                    <legend>Edit User</legend>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="product_name">Product Name</label>
                                        <div class="col-md-6">
                                            <input id="product_name" name="product_name" type="text" placeholder="" class="form-control input-md" value="<?= $product->product_name; ?>"  >

                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="product_keyword">Product Keyword</label>
                                        <div class="col-md-6">
                                            <input id="product_keyword" name="product_keyword" type="text" value="<?= $product->product_keyword; ?>" class="form-control input-md"   >

                                        </div>
                                    </div>

                                    <!-- File Button -->


                                    <!-- Button (Double) -->
                                    <div class="form-group center" style="clear:left;clear: right">
                                        <label class="col-md-6 control-label" for="update"></label>
                                        <div class="col-md-8">
                                            <button id="update" name="update" class="btn button_blue" value="update">update</button>
                                        </div>
                                    </div>
                                </fieldset>
                                <!-- Button -->
                            </form>
            </div>
        </section>

    </aside>

</div>
<script>
    document.getElementById("company_tab").className = "treeview active";
    document.getElementById("user_list").className = "active";
</script>
<?php
include 'inc/footer.php';
?>