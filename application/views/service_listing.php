ِ<?php include 'inc/header.php';
?>

<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->


        <section class="content">
            <h2 class="page-header">Services</h2>
            <div class="row">
                <div class="col-md-12">
                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab" style="font-weight: 600">Pending</a></li>
                            <li><a href="#tab_2" data-toggle="tab" style="font-weight: 600">Approved</a></li>
                            <li><a href="#tab_3" data-toggle="tab" style="font-weight: 600">Rejected</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">

                                <div class="box">

                                    <div class="box-body table-responsive">
                                        <table id="example1" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Product Name</th>
                                                    <th>Category</th>
                                                    <th>Model</th>
                                                    <th>Status</th>
                                                    <th style="text-align: center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                //print_r($countries);
                                                foreach ($pending_services as $values) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $values->product_id; ?></td>
                                                        <td><?= $values->product_name; ?></td>
                                                        <?php
                                                        foreach ($categories as $cat) {

                                                            if ($values->product_category === $cat->category_id) {
                                                                echo "<td>" . $cat->category_name . "</td>";
                                                            }
                                                        }
                                                        ?>

                                                        <td><?= $values->product_model; ?></td>
                                                        <td><span class='label label-warning'>Pending</span></td>

                                                        <td>
                                                            <a href="<?php echo base_url() ?>index.php/system/reject_product/<?php echo $values->product_id ?>" class="btn btn-sm btn-danger" style="float:right;" ><span > Reject </span></a>
                                                            <a href="<?php echo base_url() ?>index.php/system/approve_product/<?php echo $values->product_id ?>" class="btn btn-sm btn-success" style="float:right;"><span>Approve</span></a>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div><!-- /.box-body -->
                                </div><!-- /.<!-- /.box -->

                            </div><!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">
                                <div class="box">

                                    <div class="box-body table-responsive">
                                        <table id="table2" class="table table-bordered table-striped">
                                            <thead>

                                                <tr>
                                                    <th>ID</th>
                                                    <th>Product Name</th>
                                                    <th>Category</th>
                                                    <th>Model</th>
                                                    <th>Status</th>
                                                    <th style="width: 100px">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
//print_r($countries);
                                                foreach ($active_services as $valuesactive) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $valuesactive->product_id; ?></td>
                                                        <td><?= $valuesactive->product_name; ?></td>
                                                        <?php
                                                        foreach ($categories as $cat) {

                                                            if ($valuesactive->product_category === $cat->category_id) {
                                                                echo "<td>" . $cat->category_name . "</td>";
                                                            }
                                                        }
                                                        ?>

                                                        <td><a href="#"><?= $values->product_model; ?></a></td>
                                                        <td><a href="#"><span class='label label-success'>Approved</span></a></td>

                                                        <td style="width: 130px">
                                                            <a href="<?php echo base_url() ?>index.php/system/pending_product/<?php echo $valuesactive->product_id ?>" class="btn btn-sm btn-warning" style="float:right;"><span>Pending</span></a>
                                                            <a href="<?php echo base_url() ?>index.php/system/reject_product/<?php echo $valuesactive->product_id ?>" class="btn btn-sm btn-danger" style="float:right;"><span>Reject</span></a>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div><!-- /.box-body -->
                                </div><!-- /.box -->
                            </div><!-- /.tab-pane -->

                            <div class="tab-pane" id="tab_3">
                                <div class="box">

                                    <div class="box-body table-responsive">
                                        <table id="table3" class="table table-bordered table-striped">
                                            <thead>

                                                <tr>
                                                    <th>ID</th>
                                                    <th>Product Name</th>
                                                    <th>Category</th>
                                                    <th>Model</th>
                                                    <th>Status</th>
                                                    <th style="width: 100px">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
//print_r($countries);
                                                foreach ($rejected_services as $valuesrejected) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $valuesrejected->product_id; ?></td>
                                                        <td><?= $valuesrejected->product_name; ?></td>
                                                        <?php
                                                        foreach ($categories as $cat) {

                                                            if ($valuesrejected->product_category === $cat->category_id) {
                                                                echo "<td>" . $cat->category_name . "</td>";
                                                            }
                                                        }
                                                        ?>

                                                        <td><a href="#"><?= $values->product_model; ?></a></td>
                                                        <td><a href="#"><span class='label label-success'>Approved</span></a></td>

                                                        <td style="width: 130px">
                                                            <a href="<?php echo base_url() ?>index.php/system/pending_product/<?php echo $valuesrejected->product_id ?>" class="btn btn-sm btn-warning" style="float:right;"><span>Pending</span></a>
                                                            <a href="<?php echo base_url() ?>index.php/system/approve_product/<?php echo $valuesrejected->product_id ?>" class="btn btn-sm btn-success" style="float:right;"><span>Approve</span></a>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div><!-- /.box-body -->
                                </div><!-- /.box -->
                            </div>
                        </div><!-- /.tab-content -->
                    </div><!-- nav-tabs-custom -->
                </div><!-- /.col -->
            </div> <!-- /.row -->ٖ
        </section>

    </aside>

</div>
<script>
    document.getElementById("product_tab").className = "treeview active";
    document.getElementById("company_service").className = "active";
</script>
<?php
include 'inc/footer.php';
?>