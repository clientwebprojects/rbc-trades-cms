<?php
include 'inc/header.php';
$var = json_encode($all_category);
$varmain = json_encode($category);
// echo '<pre>';
// print_r($category);
// exit();
?>
<script>
    var sub_category_object = <?= $var; ?>;
    var category_object = <?= $varmain; ?>;
    var count = 0;
    var catId;
    var catName;
    //console.log(sub_category_object);

</script>

<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->


<!--href="<?= base_url() ?>index.php/system/add_category"-->




        <section class="content">
            <a href="<?= base_url() ?>index.php/system/add_category" data-toggle="modal"  class="btn btn-success pull-right">Add New Category</a>
            <h2 class="page-header">Category Management</h2>
            <div class="form-group">
                <div class="col-md-12">
                    <div class="radio">
                        <label id="forProduct">
                            <input type="radio" name="ctype" value="P">
                            For Products
                        </label>

                    </div>
                    <div class="radio">
                        <label id="forService">
                            <input type="radio" name="ctype" value="S">
                            For Services
                        </label>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div id="catProduct">
                <input type="hidden" id="newparentid" name="parent_id">
                <div class="form-group" id="catSelect">
                    <div class="col-md-4">
                        <select id="0" name="select" class="form-control" size="10" onchange="catFunction(this.value, this.id)" >
                            <?php foreach ($category as $values) { ?>
                                <option value="<?php echo $values->category_id; ?>" name="<?php echo $values->category_name; ?>"><?php echo $values->category_name; ?></option>

                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>

            <div id="catService" style="display: none">
                <input type="hidden" id="newparentid" name="parent_id">
                <div class="form-group" id="catSelect">
                    <div class="col-md-4">
                        <select id="0" name="select" class="form-control" size="10" onchange="catFunction(this.value, this.id)" >
                            <?php foreach ($category as $values) { ?>
                                <option value="<?php echo $values->category_id; ?>" name="<?php echo $values->category_name; ?>"><?php echo $values->category_name; ?></option>

                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">

                <a data-toggle="modal" class="btn btn-primary edit-category-btn" style="margin-top: 20px; ">Edit Category</a>

                <a id="delbtn" onclick="confirmDelete(this.id)" class="btn btn-danger" style="margin-top: 20px; ">Delete Category</a>
            </div>
        </section>

    </aside>

</div>
<script src="<?= base_url() ?>js/category-listing.js" type="text/javascript"></script>
<script>
                    function catFunction(panelid, id) {

                        hasSub = 0;
                        catId = panelid;
                        document.getElementById('newparentid').setAttribute('value', catId);
//        console.log(catId);
                        if (id === '0') {
                            $.each(category_object, function(i, v) {
                                if (parseInt(v.category_id) === parseInt(panelid)) {

                                    catName = v.category_name;
                                }

                            });

                        }
                        else {
                            $.each(sub_category_object, function(i, v) {


                                if (parseInt(v.category_id) === parseInt(panelid)) {

                                    catName = v.category_name;
                                }

                            });
                        }
                        if (count > id) {
                            i = count;
                            while (i > id) {
                                $('#main' + i).remove();
                                i--;
                            }
                            count = id;

                        }
                        count++;

                        $.each(sub_category_object, function(i, v) {

                            if (parseInt(v.parent_id) === parseInt(panelid)) {
                                hasSub++;
                            }

                        });


                        var start_html =
                                '<div class="col-md-4" id="main' + count + '"> ' +
                                '<select id="' + count + '" name="select' + count + '" class="form-control" size="10" onchange="catFunction(this.value,this.id)" >';
                        var end_html =
                                '</select>' +
                                '</div>';
                        console.log(hasSub);
                        if (hasSub > 0) {
                            $('#catSelect').append(start_html);

                            $.each(sub_category_object, function(i, v) {

                                if (parseInt(v.parent_id) === parseInt(panelid)) {

                                    var subCategory_html =
                                            '<option value="' + v.category_id + '">' +
                                            v.category_name +
                                            '</option>';

                                    $('#' + count).append(subCategory_html);

                                }

                            });
                            $('#catSelect').append(end_html);
                        }
                        $("#loader").hide();
                    }
</script>
<script>
    document.getElementById("website_tab").className = "treeview active";
    document.getElementById("category_management").className = "active";
</script>
<?php
include 'inc/modals.php';
include 'inc/footer.php';
?>