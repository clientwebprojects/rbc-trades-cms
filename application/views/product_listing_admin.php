ِ<?php include 'inc/header.php';
?>

<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'inc/left_menu.php'; ?>
    <aside class="right-side">
        <section class="content">
            <a href="<?= base_url() ?>index.php/system/add_new_product" class="btn btn-primary pull-right add_new_user_btn">Add New Product</a>
            <h2 class="page-header">Product Added By Admin</h2>
            <div class="row">
                <div class="col-md-12">
                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab" style="font-weight: 600">Pending</a></li>
                            <li><a href="#tab_2" data-toggle="tab" style="font-weight: 600">Approved</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">

                                <div class="box">

                                    <div class="box-body table-responsive">
                                        <table id="example1" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Product Name</th>
                                                    <th>Category</th>
                                                    <th>Model</th>
                                                    <th>Status</th>
                                                    <th style="text-align: center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                //print_r($countries);
                                                foreach ($pending_products as $values) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $values->product_id; ?></td>
                                                        <td><?= $values->product_name; ?></td>
                                                        <?php
                                                        foreach ($categories as $cat) {

                                                            if ($values->product_category === $cat->category_id) {
                                                                echo "<td>" . $cat->category_name . "</td>";
                                                            }
                                                        }
                                                        ?>

                                                        <td><a href="#"><?= $values->product_model; ?></a></td>
                                                        <td><a href="#"><span class='label label-warning'>Pending</span></a></td>

                                                        <td>
                                                            <a href="<?php echo base_url() ?>index.php/system/delete_admin_product/<?php echo $values->product_id ?>" class="btn btn-sm btn-danger" style="float:right;" ><span > Delete </span></a>
                                                            <a href="<?php echo base_url() ?>index.php/system/approve_product_admin/<?php echo $values->product_id ?>" class="btn btn-sm btn-success" style="float:right;"><span>Approve</span></a>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div><!-- /.box-body -->
                                </div><!-- /.<!-- /.box -->

                            </div><!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">
                                <div class="box">

                                    <div class="box-body table-responsive">
                                        <table id="table2" class="table table-bordered table-striped">
                                            <thead>

                                                <tr>
                                                    <th>ID</th>
                                                    <th>Product Name</th>
                                                    <th>Category</th>
                                                    <th>Model</th>
                                                    <th>Status</th>
                                                    <th style="width: 100px">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
//print_r($countries);
                                                foreach ($active_products as $valuesactive) {
                                                    ?>
                                                    <tr>
                                                        <td><a href="#"><?php echo $valuesactive->product_id; ?></a></td>
                                                        <td><a href="#"><?= $valuesactive->product_name; ?></a></td>
                                                        <?php
                                                        foreach ($categories as $cat) {

                                                            if ($valuesactive->product_category === $cat->category_id) {
                                                                echo "<td>" . $cat->category_name . "</td>";
                                                            }
                                                        }
                                                        ?>

                                                        <td><a href="#"><?= $values->product_model; ?></a></td>
                                                        <td><a href="#"><span class='label label-success'>Approved</span></a></td>

                                                        <td style="width: 130px">
                                                            <a href="<?php echo base_url() ?>index.php/system/pending_product_admin/<?php echo $valuesactive->product_id ?>" class="btn btn-sm btn-warning" style="float:right;"><span>Pending</span></a>
                                                            <a href="<?php echo base_url() ?>index.php/system/edit_admin_product/<?php echo $valuesactive->product_id ?>" class="btn btn-sm btn-danger" style="float:right;"><span>Edit</span></a>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div><!-- /.box-body -->
                                </div><!-- /.box -->
                            </div><!-- /.tab-pane -->


                        </div><!-- /.tab-content -->
                    </div><!-- nav-tabs-custom -->
                </div><!-- /.col -->


            </div> <!-- /.row -->ٖ
        </section>

    </aside>

</div>
<script>
    document.getElementById("product_tab").className = "treeview active";
    document.getElementById("company_product").className = "active";
</script>
<?php
include 'inc/footer.php';
?>