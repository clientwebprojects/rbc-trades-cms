<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        error_reporting(E_ALL & ~E_NOTICE);
        if (!$this->is_logged_in_rbc_cms()) {
            redirect('login');
        }
    }
    private function is_logged_in_rbc_cms()
    {
        return $this->session->userdata('is_logged_in_rbc_cms');
    }

    public function index() {
        $this->load->model('rbc_user_model');
        $this->load->model('product_master_model');
        $data['register_users'] = $this->rbc_user_model->get_all_users();
        $data['pending_products'] = $this->product_master_model->get_count_pending_products();
        $data['pending_services'] = $this->product_master_model->get_count_pending_services();
        $this->load->view('index',$data);
    }

    public function category_listing() {

        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            $this->load->model('rbc_category_model');
            $data['category'] = $this->rbc_category_model->get_all_parent_id($id);

            $phpVar     = array( "STATUS" => $data['category'] );
            echo json_encode ($data['category']) ;
        } else {
            $this->load->model('rbc_category_model');
            $data['category'] = $this->rbc_category_model->get_by_category();
            $data['all_category'] = $this->rbc_category_model->get_by_sub_category();

            $this->load->view('category_listing', $data);
        }
    }

    public function get_sub_category() {

    }

       public function edit_category() {
//        $category_id = $this->uri->segment(3);
//        $this->load->model('rbc_category_model');
//        $data['category'] = $this->rbc_category_model->get_by_id($category_id);
//        $this->load->view('edit_category',$data);
            $this->load->view('edit_category');
        }
        public function add_category() {
//        $category_id = $this->uri->segment(3);
//        $this->load->model('rbc_category_model');
//        $data['category'] = $this->rbc_category_model->get_by_id($category_id);
//        $this->load->view('edit_category',$data);
            $this->load->view('add_category');
        }


}
