<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        error_reporting(E_ALL & ~E_NOTICE);
    }

    private function is_logged_in_rbc_cms() {
        return $this->session->userdata('is_logged_in_rbc_cms');
    }

    public function index() {
        if (!$this->is_logged_in_rbc_cms()) {
            $this->load->view('login');
        } else {
            redirect('dashboard');
        }
    }

    public function check() {
//        $this->load->model('member_master_model');
//        $this->load->library('form_validation');
//        $this->form_validation->set_rules('user', 'User Name', 'required');
//        $this->form_validation->set_rules('pwd', 'Password', 'required');
//        //exit($this->input->post('user'));
//        if ($this->form_validation->run() == FALSE) {
//            redirect('login/error');
//        } else {
//            $user = $this->rbc_user_model->check_user($this->input->post('user'), $this->input->post('pwd'));
//            $company_id = $this->member_master_model->get_company_id($user->member_id);
        $user = $this->input->post('user');
        $password = $this->input->post('pwd');
        if ($user == "admin" && $password == "admin") {
//                $user_name = explode(' ', $user->user_name);
            $data = array(
//                    'user_name' => $user_name[0],
//                    'user_id' => $user->user_id,
//                    'member_id' => $user->member_id,
//                    'company_id' => $company_id->company_id,
//                    'email' => $user->user_email,
//                    'name' => $user->user_name,
                'is_logged_in_rbc_cms' => TRUE
            );
            $this->session->set_userdata($data);
            //$referred_from = $this->session->userdata('referred_from');
            redirect('dashboard/index');
        } else {
            redirect('login/error');
        }
        //}
    }

    public function error() {
        $this->load->view('login', array('error' => TRUE));
    }

    public function logout() {
        if (!$this->is_logged_in_rbc_cms()) {
            redirect('login');
        } else {
            $this->session->set_userdata(array('is_logged_in_rbc_cms' => FALSE));
            $this->session->sess_destroy();
            $this->load->view('login');
        }
    }

}

/*End of file login.php*/
