<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class system extends CI_Controller {
    public function __construct() {
        parent::__construct();
        error_reporting(E_ALL & ~E_NOTICE);
        if (!$this->is_logged_in_rbc_cms()) {
            redirect('login');
        }
    }
    private function is_logged_in_rbc_cms()
    {
        return $this->session->userdata('is_logged_in_rbc_cms');
    }
    public function index() {
        $this->load->view('index');
    }

    public function view_buying_leeds() {
        $this->load->view('buying_leeds');
    }

    public function category_listing() {

        $this->load->model('rbc_category_model');
        $data['category'] = $this->rbc_category_model->get_by_category();
        $data['all_category'] = $this->rbc_category_model->get_by_sub_category();

        $this->load->view('category_listing', $data);
        if ($this->input->post('edit_category')) {
            $data = $_POST;
            $id = $this->input->post('category_id');
            $this->rbc_category_model->update($id, $data);
            redirect('/system/category_listing');
        }
    }

    public function edit_category() {
//        $category_id = $this->uri->segment(3);
//        $this->load->model('rbc_category_model');
//        $data['category'] = $this->rbc_category_model->get_by_id($category_id);
//        $this->load->view('edit_category',$data);
        $this->load->view('edit_category');
    }

    public function product_listing() {
        $this->load->model('product_master_model');
        $this->load->model('rbc_category_model');
        $data['pending_products'] = $this->product_master_model->get_products_by_status('P','P');
        $data['active_products'] = $this->product_master_model->get_products_by_status('P','A');
        $data['rejected_products'] = $this->product_master_model->get_products_by_status('P','R');
        $data['categories'] = $this->rbc_category_model->get_all();
//            print_r($data);exit();
        $this->load->view('product_listing', $data);
    }
    public function service_listing() {
        $this->load->model('product_master_model');
        $this->load->model('rbc_category_model');
        $data['pending_services'] = $this->product_master_model->get_products_by_status('S','P');
        $data['active_services'] = $this->product_master_model->get_products_by_status('S','A');
        $data['rejected_services'] = $this->product_master_model->get_products_by_status('S','R');
        $data['categories'] = $this->rbc_category_model->get_all();
//            print_r($data);exit();
        $this->load->view('service_listing', $data);
    }

    public function product_listing_admin() {
        $this->load->model('product_master_model');
        $this->load->model('product_member_model');
        $this->load->model('rbc_category_model');
        $member_id = $this->session->userdata('member_id');

        $data['pending_products'] = $this->product_master_model->get_all_pending_admin_products($member_id);

        $data['active_products'] = $this->product_master_model->get_all_active_admin_products($member_id);

        $data['categories'] = $this->rbc_category_model->get_all();
//            print_r($data);exit();
        $this->load->view('product_listing_admin', $data);
    }

    public function add_new_product() {
        $this->load->model('product_member_model');
        $this->load->model('product_master_model');
        $this->load->model('rbc_category_model');
        $this->load->model('product_images_model');
        $this->load->model('rbc_uom_model');
        $this->load->model('rbc_countries_model');
        $this->load->model('rbc_payment_type_model');
        $data['payment_types'] = $this->rbc_payment_type_model->get_all();
        $data['category'] = $this->rbc_category_model->get_by_category();
        $data['all_category'] = $this->rbc_category_model->get_by_sub_category();
        $data['countries'] = $this->rbc_countries_model->get_all();
        $data['uom'] = $this->rbc_uom_model->get_all();
//            echo '<pre>';
//            print_r($data['uom']);exit();
        $this->load->view('product_add', $data);
        if ($_POST) {

//                echo '<pre>';
//                echo print_r($_FILES);
//                 echo print_r($_POST);
            //$this->rbc_user_model->get_by_id($this->session->userdata('uid'));
            //print_r($_post);exit();
            $payment_type = $this->input->post('product_payment_type');
            $payment_type_value = "";
            foreach ($payment_type as $values) {
                if (!empty($values)) {
                    $payment_type_value .= $values;
                    $payment_type_value .= "|";
                }
            }
            //print_r($payment_type_value);exit();
            $product = $_POST;
            $product['product_payment_type'] = $payment_type_value;
            $data['member_id'] = $this->session->userdata('member_id');

            $product_id = $this->product_master_model->create($product);
            $count = 0;
            $upload_path = 'uploads/product_images/';
            if (!is_dir($upload_path)) {
                mkdir($upload_path, 0777, TRUE);
            }
            for ($i = 0; $i < sizeof($_FILES['product_image']['name']); $i++) {
                if (!empty($_FILES['product_image']['name'][$i]) && $_FILES['product_image']['error'][$i] == 0) {
                    $type = explode(".", $_FILES['product_image']['name'][$i]);

                    $_FILES['product_image']['name'][$i] = $this->random_string() . "." . $type[1];

                    move_uploaded_file($_FILES['product_image']['tmp_name'][$i], $upload_path . $_FILES['product_image']['name'][$i]);
                    $images['image_url'] = base_url() . $upload_path . $_FILES['product_image']['name'][$i];
                    $images['product_id'] = $product_id;
                    $this->product_images_model->create($images);
                    //redirect('/seller/manage_products/');
                }
            }

            $data['product_id'] = $product_id;
            $this->product_member_model->create($data);
            //redirect('/seller/manage_products/');
        }
    }

    public function edit_admin_product() {

        $this->load->model('rbc_category_model');
        $this->load->model('product_master_model');
        $this->load->model('product_images_model');
        $this->load->model('rbc_uom_model');
        $this->load->model('rbc_countries_model');
        $this->load->model('rbc_payment_type_model');
        $data['payment_types'] = $this->rbc_payment_type_model->get_all();
        $data['countries'] = $this->rbc_countries_model->get_all();

        $product_id = $this->uri->segment(3);
        $data['category'] = $this->rbc_category_model->get_by_category();
        $data['all_category'] = $this->rbc_category_model->get_by_sub_category();
        $data['product'] = $this->product_master_model->get_by_id($product_id);
        $data['uom'] = $this->rbc_uom_model->get_all();

        $data['images'] = $this->product_images_model->get_by_product_id($product_id);
        //echo print_r($data['images']);
//              print_r($data['product']);exit();
        $this->load->view('product_edit', $data);

        if ($this->input->post('update')) {
            $payment_type = $this->input->post('product_payment_type');
            $payment_type_value = "";
            foreach ($payment_type as $values) {
                if (!empty($values)) {
                    $payment_type_value .= $values;
                    $payment_type_value .= "|";
                }
            }
            $product = $_POST;
            //print_r($product);exit();
            $product['product_payment_type'] = $payment_type_value;
            $this->product_master_model->update($product_id, $product);
            $count = 0;
            $upload_path = 'uploads/product_images/';
            if (!is_dir($upload_path)) {
                mkdir($upload_path, 0777, TRUE);
            }
            for ($i = 0; $i < sizeof($_FILES['product_image']['name']); $i++) {
                if (!empty($_FILES['product_image']['name'][$i]) && $_FILES['product_image']['error'][$i] == 0) {
                    $type = explode(".", $_FILES['product_image']['name'][$i]);

                    $_FILES['product_image']['name'][$i] = $this->random_string() . "." . $type[1];

                    move_uploaded_file($_FILES['product_image']['tmp_name'][$i], $upload_path . $_FILES['product_image']['name'][$i]);
                    $images['image_url'] = base_url() . $upload_path . $_FILES['product_image']['name'][$i];
                    $images['product_id'] = $product_id;
                    $this->product_images_model->create($images);
                    //redirect('/seller/manage_products/');
                }
            }
            redirect('/system/product_listing_admin/');
        }
    }

    public function add_category() {
        $this->load->model('rbc_category_model');
        $data['category'] = $this->rbc_category_model->get_by_category();
        $data['all_category'] = $this->rbc_category_model->get_by_sub_category();

        $this->load->view('add_category', $data);
//           if(isset($_POST)){
//               print_r($_POST);exit();
//           }else
//           {
//               echo "Not Set";exit();
//           }
        if ($this->input->post('add_category_form')) {
//                print_r($_POST);exit();
            $catdata['category_name'] = $this->input->post('category_name_textfield');
            $catdata['parent_id'] = $this->input->post('parent_id');
            $this->rbc_category_model->create($catdata);
            redirect('/system/add_category');
        }
    }

    public function add_news() {
        $this->load->model('rbc_news_model');
    $this->load->view('add_news');
        if ($this->input->post('add-news-submit')) {
            $data = $_POST;
            $data = $_FILES;
//            echo'<pre>';
//            print_r($_POST);
//                        exit();

            $data['news_title'] = $this->input->post('news_title');
            $data['news_description'] = $this->input->post('news_description');
            $data['news_date'] = date('Y-m-d');

            //$count = 0;
            $upload_path = 'uploads/news_images/';
            if (!is_dir($upload_path)) {
                mkdir($upload_path, 0777, TRUE);
            }
            if (!empty($_FILES['news_image']['name']) && $_FILES['news_image']['error'] == 0) {
                $type = explode(".", $_FILES['news_image']['name']);

                $_FILES['news_image']['name'] = $this->random_string() . "." . $type[1];

                move_uploaded_file($_FILES['news_image']['tmp_name'], $upload_path . $_FILES['news_image']['name']);
                $data['news_image'] = base_url() . $upload_path . $_FILES['news_image']['name'];
                $this->rbc_news_model->create($data);
            }
            redirect('/system/news_listing');
        }
    }

    public function add_new_currency() {
        $this->load->model('rbc_currency_model');
    $this->load->view('add_new_currency');

        if ($this->input->post('add-currency-submit')) {
            $data = $_POST;
            $data['currency_name'] = $this->input->post('currency_name');
            $data['currency_code'] = $this->input->post('currency_code');
            $data['currency_symbol'] = $this->input->post('currency_symbol');

                $this->rbc_currency_model->create($data);
                redirect('/system/currency_listing');
            }

        }
        public function edit_currency() {
        $this->load->model('rbc_currency_model');
        $currency_id = $this->uri->segment(3);
        $data['edit_currency'] = $this->rbc_currency_model->get_by_id($currency_id);
        $this->load->view('edit_currency',$data);

        if ($this->input->post('edit-currency-submit')) {
            $data = $_POST;
             //echo print_r($data);exit;
            $data['currency_name'] = $this->input->post('currency_name');
            $data['currency_code'] = $this->input->post('currency_code');
            $data['currency_symbol'] = $this->input->post('currency_symbol');

                $this->rbc_currency_model->update($currency_id,$data);
                redirect('/system/currency_listing');
            }

        }
    public function currency_listing() {
        $this->load->model('rbc_currency_model');
        $data['currency_listing'] = $this->rbc_currency_model->get_all();

        $this->load->view('currency_listing', $data);

    }

    public function country_listing() {
        $this->load->model('rbc_countries_model');
        $data['countries'] = $this->rbc_countries_model->get_all();
        $this->load->view('country_listing', $data);
        if ($this->input->post('add_country')) {
            $data = $_POST;
            $this->rbc_countries_model->create($data);
            redirect('/system/country_listing');
        }
        if ($this->input->post('edit_country')) {
            $data = $_POST;
            $id = $this->input->post('country_id');
            $this->rbc_countries_model->update($id, $data);
            redirect('/system/country_listing');
        }
    }
     public function payment_term_listing() {
        $this->load->model('rbc_payment_type_model');
        $data['payments'] = $this->rbc_payment_type_model->get_all();
        $this->load->view('payment_listing', $data);
        if ($this->input->post('add_payment_term')) {
            $data = $_POST;
            //echo print_r($_POST);exit;
            $this->rbc_payment_type_model->create($data);
            redirect('/system/payment_term_listing');
        }
        if ($this->input->post('edit_payment')) {
            $data = $_POST;
            //echo print_r($data);exit;
            $p_term_id = $this->input->post('p_term_id');
            $this->rbc_payment_type_model->update($p_term_id, $data);
            redirect('/system/payment_term_listing');
        }
    }
    public function news_listing() {
        $this->load->model('rbc_news_model');
        $data['news_listing'] = $this->rbc_news_model->get_all();
        $this->load->view('rbc_news_listing', $data);

        if ($this->input->post('edit_news')) {
            $data = $_POST;
            $id = $this->input->post('news_id');
            $this->rbc_countries_model->update($id, $data);
            redirect('/system/news_listing');
        }
    }public function delete_business_type() {
        $this->load->model('business_type_model');
        $business_type_id = $this->uri->segment(3);
        $this->business_type_model->delete($business_type_id);
        redirect('/system/business_type_listing');
    }

    public function delete_country() {
        $country_id = $this->uri->segment(3);
        $this->rbc_countries_model->delete($country_id);
        redirect('/system/country_listing');
    }
    public function delete_department() {
         $this->load->model('rbc_department_model');
        $id = $this->uri->segment(3);
        $this->rbc_department_model->delete($id);
        redirect('/system/department_listing');
    }
     public function delete_payment() {
          $this->load->model('rbc_payment_type_model');
        $p_term_id = $this->uri->segment(3);
        $this->rbc_payment_type_model->delete($p_term_id);
        redirect('/system/payment_term_listing');
    }
    public function delete_currency() {
        $this->load->model('rbc_currency_model');
        $currency_id = $this->uri->segment(3);
        $this->rbc_currency_model->delete($currency_id);
        redirect('/system/currency_listing');
    }
     public function delete_news() {
         $this->load->model('rbc_news_model');
        $news_id = $this->uri->segment(3);
        $this->rbc_news_model->delete($news_id);
        redirect('/system/news_listing');
    }
    public function delete_security_question() {
         $this->load->model('rbc_security_question_model');
        $id = $this->uri->segment(3);
        $this->rbc_security_question_model->delete($id);
        redirect('/system/security_question_listing');
    }
    public function delete_product() {
        $this->load->model('product_master_model');
        $product_id = $this->uri->segment(3);
//            print_r($product_id);exit();
        $this->product_master_model->delete($product_id);
        redirect('/system/product_listing');
    }

    public function business_type_listing() {
        $this->load->model('business_type_model');
        $data['business_type'] = $this->business_type_model->get_all();
        $this->load->view('business_type_listing', $data);
        if ($this->input->post('add_business_type')) {
            $data = $_POST;
            $this->business_type_model->create($data);
            redirect('/system/business_type_listing');
        }
        if ($this->input->post('edit_business_type')) {
            $data = $_POST;
            $id = $this->input->post('business_type_id');
            $this->business_type_model->update($id, $data);
            redirect('/system/business_type_listing');
        }
    }

    public function security_question_listing() {
        $this->load->model('rbc_security_question_model');
        $data['questions'] = $this->rbc_security_question_model->get_all();
        $this->load->view('security_question_listing', $data);
        if ($this->input->post('add_security_question')) {
            $data = $_POST;
            $this->rbc_security_question_model->create($data);
            redirect('/system/security_question_listing');
        }
        if ($this->input->post('edit_security_question')) {
            $data = $_POST;
            $id = $this->input->post('id');
            $this->rbc_security_question_model->update($id, $data);
            redirect('/system/security_question_listing');
        }
    }

    public function sales_volume_listing() {
        $this->load->model('rbc_annual_sales_volume_model');
        $data['annual_sales'] = $this->rbc_annual_sales_volume_model->get_all();
        $this->load->view('sales_volume_listing', $data);
        if ($this->input->post('add_sales_volume')) {
            $data = $_POST;
            $this->rbc_annual_sales_volume_model->create($data);
            redirect('/system/sales_volume_listing');
        }
        if ($this->input->post('edit_sales_volume')) {
            $data = $_POST;
            $id = $this->input->post('id');
            $this->rbc_annual_sales_volume_model->update($id, $data);
            redirect('/system/sales_volume_listing');
        }
    }
     public function delete_sales_volume() {
         $this->load->model('rbc_annual_sales_volume_model');
        $id = $this->uri->segment(3);
        $this->rbc_annual_sales_volume_model->delete($id);
        redirect('/system/sales_volume_listing');
    }

    public function purchasing_frequency_listing() {
        $this->load->model('rbc_purchasing_frequency_model');
        $data['purchasing'] = $this->rbc_purchasing_frequency_model->get_all();
        $this->load->view('purchasing_frequency_listing', $data);
        if ($this->input->post('add_purchasing_frequency')) {
            $data = $_POST;
            $this->rbc_purchasing_frequency_model->create($data);
            redirect('/system/purchasing_frequency_listing');
        }
        if ($this->input->post('edit_purchasing_frequency')) {
            $data = $_POST;
            $id = $this->input->post('id');
            $this->rbc_purchasing_frequency_model->update($id, $data);
            redirect('/system/purchasing_frequency_listing');
        }
    }
     public function delete_purchasing_frequency() {
        $this->load->model('rbc_purchasing_frequency_model');
        $frequency_id = $this->uri->segment(3);
        $this->rbc_purchasing_frequency_model->delete($frequency_id);
        redirect('/system/purchasing_frequency_listing');
    }
    public function department_listing() {
        $this->load->model('rbc_department_model');
        $data['departments'] = $this->rbc_department_model->get_all();
        $this->load->view('department_listing', $data);
        if ($this->input->post('add_department')) {
            $data = $_POST;
            $this->rbc_department_model->create($data);
            redirect('/system/department_listing');
        }
        if ($this->input->post('edit_department')) {
            $data = $_POST;
            $id = $this->input->post('id');
            $this->rbc_department_model->update($id, $data);
            redirect('/system/department_listing');
        }
    }
    public function delete_admin_product() {
        $this->load->model('product_master_model');
        $product_id = $this->uri->segment(3);
//            print_r($product_id);exit();
        $this->product_master_model->delete($product_id);
        redirect('/system/product_listing_admin');
    }

    public function approve_product() {
        $this->load->model('product_master_model');
        $product_id = $this->uri->segment(3);
        // print_r($product_id);exit();
        $this->product_master_model->product_approve($product_id);
        redirect('/system/product_listing');
    }

    public function approve_product_admin() {
        $this->load->model('product_master_model');
        $product_id = $this->uri->segment(3);
        // print_r($product_id);exit();
        $this->product_master_model->product_approve($product_id);
        redirect('/system/product_listing_admin');
    }

    public function reject_product() {
        $this->load->model('product_master_model');
        $product_id = $this->uri->segment(3);
        // print_r($product_id);exit();
        $this->product_master_model->product_reject($product_id);
        redirect('/system/product_listing');
    }

    public function pending_product() {
        $this->load->model('product_master_model');
        $product_id = $this->uri->segment(3);
        // print_r($product_id);exit();
        $this->product_master_model->product_pending($product_id);
        redirect('/system/product_listing');
    }

    public function pending_product_admin() {
        $this->load->model('product_master_model');
        $product_id = $this->uri->segment(3);
        // print_r($product_id);exit();
        $this->product_master_model->product_pending($product_id);
        redirect('/system/product_listing_admin');
    }

    public function delete_category() {
        $cat_id = $this->uri->segment(3);
        $this->rbc_category_model->delete_sub_categories($cat_id);
        $this->rbc_category_model->delete($cat_id);
        redirect('/system/category_listing');
    }

    public function image_delete() {

        $this->load->model('product_images_model');
        $image_id = $this->input->post('image_id');
        //echo print_r($image_id);exit;
        $get_image_data = $this->product_images_model->get_by_id($image_id);
        $img_url = $get_image_data->image_url;
        $img_name = explode('/', $img_url);
        $image_name = $img_name['6'];
        $image_path = '../rbctrades/uploads/product_images/' . $image_name;
        unlink($image_path);
        $this->product_images_model->delete_image($image_id);
        //redirect('/seller/manage_products/');
        exit();
    }

    PUBLIC function random_string() {
        $length = 6;
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

    public function slider_banner() {
        $this->load->model('rbc_home_slider_model');
        $data['slider_images'] = $this->rbc_home_slider_model->get_all();
        $this->load->view('slider_banner', $data);
    }

    public function add_new_slider_images() {
        $this->load->model('rbc_home_slider_model');
        $this->load->view('add_slider_banner');
        if ($this->input->post('add-slider-submit')) {
            $data = $_POST;
            $data = $_FILES;
//            echo'<pre>';
//            print_r($_FILES);
//                        exit();
            $data['image_description'] = $this->input->post('Image_description');
            $count = 0;
            $upload_path = 'uploads/home_slider/';
            if (!is_dir($upload_path)) {
                mkdir($upload_path, 0777, TRUE);
            }
            for ($i = 0; $i < sizeof($_FILES['slider_image']['name']); $i++) {
                if (!empty($_FILES['slider_image']['name'][$i]) && $_FILES['slider_image']['error'][$i] == 0) {
                    $type = explode(".", $_FILES['product_image']['name'][$i]);

                    $_FILES['slider_image']['name'][$i] = $this->random_string() . "." . $type[1];

                    move_uploaded_file($_FILES['slider_image']['tmp_name'][$i], $upload_path . $_FILES['slider_image']['name'][$i]);
                    $data['image_path'] = base_url() . $upload_path . $_FILES['slider_image']['name'][$i];
                    $this->rbc_home_slider_model->create($data);
                }
            }
            redirect('/system/slider_banner');
        }
    }

    public function category_banner() {
        $this->load->model('rbc_banners_model');
        $this->load->model('rbc_category_model');
        $this->load->model('rbc_banner_locations_model');
        $data['banners'] = $this->rbc_banners_model->get_all_data();
        $this->load->view('category_banner', $data);
    }

    public function add_category_banner() {
        $this->load->model('rbc_banners_model');
        $this->load->model('rbc_category_model');
        $this->load->model('rbc_banner_locations_model');
		$data['categories'] = $this->rbc_category_model->get_category_for_menu();

        $data['category'] = $this->rbc_category_model->get_by_category();
        $data['banner_location'] = $this->rbc_banner_locations_model->get_all();
        $this->load->view('add_category_banner', $data);
        if ($this->input->post('add-banner-submit')) {
            $data = $_POST;
            $data = $_FILES;

            $data['banner_description'] = $this->input->post('banner_description');
            $data['location_id'] = $this->input->post('location_id');
            $data['category_location'] = $this->input->post('category_location');
            $upload_path = 'uploads/banner_images/';
            if (!is_dir($upload_path)) {
                mkdir($upload_path, 0777, TRUE);
            }

            if (!empty($_FILES['banner_image']['name']) && $_FILES['banner_image']['error'] == 0) {
                $type = explode(".", $_FILES['banner_image']['name']);

                $_FILES['banner_image']['name'] = $this->random_string() . "." . $type[1];

                move_uploaded_file($_FILES['banner_image']['tmp_name'], $upload_path . $_FILES['banner_image']['name']);
                $data['image_path'] = base_url() . $upload_path . $_FILES['banner_image']['name'];
                $this->rbc_banners_model->create($data);
                $this->load->view('category_banner');
            }
        }
    }

    public function delete_banner() {
        $banner_id = $this->uri->segment(3);
        $this->load->model('rbc_banners_model');
        $this->rbc_banners_model->delete($banner_id);
        redirect('/system/category_banner');
    }

    public function delete_slider_image() {
        $image_slider_id = $this->uri->segment(3);
        $this->load->model('rbc_home_slider_model');
        $this->rbc_home_slider_model->delete($image_slider_id);
        redirect('/system/slider_banner');
    }

    public function edit_news() {
        $news_id = $this->uri->segment(3);
        $this->load->model('rbc_news_model');

        $data['edit_news'] = $this->rbc_news_model->get_by_id($news_id);
        $this->load->view('edit_news_view', $data);
        if ($this->input->post('edit-news-submit')) {
            $data = $_POST;
            $data = $_FILES;

            $data['news_title'] = $this->input->post('news_title');
            $data['news_description'] = $this->input->post('news_description');
//            $data['news_date'] = $this->input->post('datepicker');
            $upload_path = 'uploads/news_images/';
            if (!is_dir($upload_path)) {
                mkdir($upload_path, 0777, TRUE);
            }

            if (!empty($_FILES['news_image']['name']) && $_FILES['news_image']['error'] == 0) {
                $type = explode(".", $_FILES['news_image']['name']);

                $_FILES['news_image']['name'] = $this->random_string() . "." . $type[1];

                move_uploaded_file($_FILES['news_image']['tmp_name'], $upload_path . $_FILES['news_image']['name']);
                $data['news_image'] = base_url() . $upload_path . $_FILES['news_image']['name'];

                $this->rbc_news_model->update($news_id, $data);
                redirect('/system/news_listing');
            }
        }
    }

    public function edit_category_banner() {
        $banner_id = $this->uri->segment(3);
        $this->load->model('rbc_banners_model');
        $this->load->model('rbc_category_model');
        $this->load->model('rbc_banner_locations_model');
        $data['edit_banners'] = $this->rbc_banners_model->get_all_data_by_id($banner_id);

        $data['category'] = $this->rbc_category_model->get_by_category();
        $data['banner_location'] = $this->rbc_banner_locations_model->get_all();
        $this->load->view('edit_category_banner', $data);
        if ($this->input->post('update-banner-submit')) {
            $data = $_POST;
            $data = $_FILES;

            $data['banner_description'] = $this->input->post('banner_description');
            $data['location_id'] = $this->input->post('location_id');
            $data['category_location'] = $this->input->post('category_location');
            $upload_path = 'uploads/banner_images/';
            if (!is_dir($upload_path)) {
                mkdir($upload_path, 0777, TRUE);
            }

            if (!empty($_FILES['banner_image']['name']) && $_FILES['banner_image']['error'] == 0) {
                $type = explode(".", $_FILES['banner_image']['name']);

                $_FILES['banner_image']['name'] = $this->random_string() . "." . $type[1];

                move_uploaded_file($_FILES['banner_image']['tmp_name'], $upload_path . $_FILES['banner_image']['name']);
                $data['image_path'] = base_url() . $upload_path . $_FILES['banner_image']['name'];

                $this->rbc_banners_model->update($banner_id, $data);
                redirect('system/category_banner');
            }
        }
    }
    public function export_percentage_listing() {
        $this->load->model('rbc_export_percentage_model');
        $data['all_export'] = $this->rbc_export_percentage_model->get_all();
        $this->load->view('export_percentage_listing', $data);
        if ($this->input->post('add_export_percentage')) {
            $data = $_POST;
            $this->rbc_export_percentage_model->create($data);
            redirect('/system/export_percentage_listing');
        }
        if ($this->input->post('edit_export_percentage')) {
            $data = $_POST;
            $id = $this->input->post('id');
            $this->rbc_export_percentage_model->update($id, $data);
            redirect('/system/export_percentage_listing');
        }
    }
     public function delete_export_percentage() {
         $this->load->model('rbc_export_percentage_model');
        $id = $this->uri->segment(3);
        $this->rbc_export_percentage_model->delete($id);
        redirect('/system/export_percentage_listing');
    }

    public function office_size_listing() {
        $this->load->model('rbc_office_size_model');
        $data['all_records'] = $this->rbc_office_size_model->get_all();
        $this->load->view('office_size_listing', $data);
        if ($this->input->post('add_office_size')) {
            $data = $_POST;
            $this->rbc_office_size_model->create($data);
            redirect('/system/office_size_listing');
        }
        if ($this->input->post('edit_office_size')) {
            $data = $_POST;
            $id = $this->input->post('id');
            $this->rbc_office_size_model->update($id, $data);
            redirect('/system/office_size_listing');
        }
    }
     public function delete_office_size() {
         $this->load->model('rbc_office_size_model');
        $id = $this->uri->segment(3);
        $this->rbc_office_size_model->delete($id);
        redirect('/system/office_size_listing');
    }

    public function employees_listing() {
        $this->load->model('rbc_no_of_employees_model');
        $data['all_records'] = $this->rbc_no_of_employees_model->get_all();
        $this->load->view('no_of_employees_listing', $data);
        if ($this->input->post('add_employees')) {
            $data = $_POST;
            $this->rbc_no_of_employees_model->create($data);
            redirect('/system/employees_listing');
        }
        if ($this->input->post('edit_employees')) {
            $data = $_POST;
            $id = $this->input->post('id');
            $this->rbc_no_of_employees_model->update($id, $data);
            redirect('/system/employees_listing');
        }
    }
     public function delete_employees() {
         $this->load->model('rbc_no_of_employees_model');
        $id = $this->uri->segment(3);
        $this->rbc_no_of_employees_model->delete($id);
        redirect('/system/employees_listing');
    }
    public function membership() {
        $this->load->model('rbc_membership_model');
        $data['membership'] = $this->rbc_membership_model->get_all();
        $this->load->view('membership_listing', $data);
    }
    public function membership_edit() {
        $id = $this->uri->segment(3);
        $this->load->model('rbc_membership_model');
        $this->load->model('rbc_membership_package_model');
        $data['membership_package'] = $this->rbc_membership_package_model->get_all();
        $data['membership'] = $this->rbc_membership_model->get_by_id($id);
        $this->load->view('membership_edit', $data);

        if ($this->input->post('membership_edit')) {
            $data = $_POST;
            $this->rbc_membership_model->update($data['membership_id'], $data);
            redirect('/system/membership');
        }
    }
    public function membership_package() {
        $this->load->model('rbc_membership_package_model');
        $this->load->model('rbc_currency_model');
        $data['membership_package'] = $this->rbc_membership_package_model->get_all();
        $data['currency'] = $this->rbc_currency_model->get_all();
        $data['currency2'] = $this->rbc_currency_model->get_all();
        $this->load->view('membership_package_listing', $data);
        if ($this->input->post('add_membership_package')) {
            $data = $_POST;
            $this->rbc_membership_package_model->create($data);
            redirect('/system/membership_package');
        }
        if ($this->input->post('edit_membership_package')) {
            $data = $_POST;
            $this->rbc_membership_package_model->update($data['pack_id'], $data);
            redirect('/system/membership_package');
        }
    }

}

/* End of file welcome.php */
        /* Location: ./application/controllers/welcome.php */