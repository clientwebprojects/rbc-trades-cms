<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends CI_Controller {

    public function __construct() {
        parent::__construct();
        error_reporting(E_ALL & ~E_NOTICE);
        if (!$this->is_logged_in_rbc_cms()) {
            redirect('login');
        }
    }
    private function is_logged_in_rbc_cms()
    {
        return $this->session->userdata('is_logged_in_rbc_cms');
    }
    public function index() {

        $this->load->view('new_user');
    }

    public function create_uesr(){

        $this->load->view('new_user');

    }
    public function edit_user(){

        $this->load->view('edit_user');

    }

    public function users_listing(){
        $this->load->model('rbc_user_model');
        $this->load->model('member_master_model');
        $this->load->model('rbc_companies_model');
        $this->load->model('rbc_countries_model');
        $data['active_users']= $this->rbc_user_model->get_active_users();
        $data['banned_users']= $this->rbc_user_model->get_banned_users();
        $data['countries'] = $this->rbc_countries_model->get_all_country_names();
        $this->load->view('user_listing',$data);
    }

    public function delete_user() {
        $this->load->model('rbc_user_model');
        $user_id = $this->uri->segment(3);
//            print_r($product_id);exit();
        $this->rbc_user_model->delete($user_id);
        redirect('/user/users_listing/');
    }
     public function do_bann_user(){
        $this->load->model('rbc_user_model');

        $user_id = $this->uri->segment(3);
        $this->rbc_user_model->update($user_id);
        redirect('/users/users_listing/');
    }
    public function un_bann_user(){
        $this->load->model('rbc_user_model');

        $user_id = $this->uri->segment(3);
        $this->rbc_user_model->update_unban($user_id);
        redirect('/users/users_listing/');
    }
    public function company_listing(){
        $this->load->model('rbc_companies_model');
        $data['verified_companies']= $this->rbc_companies_model->get_verfied_companies();
        $data['unverified_companies']= $this->rbc_companies_model->get_unverfied_companies();
        $this->load->view('company_listing',$data);
    }
}