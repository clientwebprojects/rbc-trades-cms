<?php
class Rbc_home_slider_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'image_path' => $item['image_path'],
			'image_description' => $item['image_description']
			 ); 
                         
		$this->db->insert('rbc_home_slider', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('rbc_home_slider');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('rbc_home_slider');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'image_path' => $item['image_path'],
			'image_description' => $item['image_description']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('rbc_home_slider', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('rbc_home_slider');
	}
}