<?php
class Rbc_membership_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'membership_name' => $item['membership_name'],
			'product_approval' => $item['product_approval'],
			'product_limit' => $item['product_limit'],
			'build_quotation_limit' => $item['build_quotation_limit'],
			'get_quotation_limit' => $item['get_quotation_limit'],
			'mini_site' => $item['mini_site'],
			'product_listing_priority' => $item['product_listing_priority'],
			'chat_messenger' => $item['chat_messenger'],
			'package_id' => $item['package_id']
			 );

		$this->db->insert('rbc_membership', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('m.*,mp.package_fee,mp.package_duration,c.currency_code,c.currency_symbol');
		$this->db->from('rbc_membership m');
                $this->db->join('rbc_membership_package mp','m.package_id = mp.id','left');
                $this->db->join('rbc_currency c','mp.currency_id = c.id','left');
		$this->db->where('membership_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('m.*,mp.package_fee,mp.package_duration,c.currency_code,c.currency_symbol');
		$this->db->from('rbc_membership m');
                $this->db->join('rbc_membership_package mp','m.package_id = mp.id','left');
                $this->db->join('rbc_currency c','mp.currency_id = c.id','left');
                $this->db->order_by('m.membership_id ASC');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'membership_name' => $item['membership_name'],
			'product_approval' => $item['product_approval'],
			'product_limit' => $item['product_limit'],
			'build_quotation_limit' => $item['build_quotation_limit'],
			'get_quotation_limit' => $item['get_quotation_limit'],
			'mini_site' => $item['mini_site'],
			'product_listing_priority' => $item['product_listing_priority'],
			'chat_messenger' => $item['chat_messenger'],
			'package_id' => $item['package_id']
			 );

		$this->db->where('membership_id', $id);
		$this->db->update('rbc_membership', $data);
	}

	function delete($id)
	{
		$this->db->where('membership_id', $id);
		$this->db->delete('rbc_membership');
	}
}