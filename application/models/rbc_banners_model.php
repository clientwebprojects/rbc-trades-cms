<?php
class Rbc_banners_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'image_path' => $item['image_path'],
			'location_id' => $item['location_id'],
			'category_location' => $item['category_location'],
			'banner_description' => $item['banner_description']
			 ); 

		$this->db->insert('rbc_banners', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('rbc_banners');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('rbc_banners');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}
        function get_all_data()
	{
		$this->db->select('*');
		$this->db->from('rbc_banners');
                $this->db->join('rbc_banner_locations','rbc_banners.location_id = rbc_banner_locations.id','left');
                $this->db->join('rbc_category','rbc_banners.category_location = rbc_category.category_id','left');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}
        function get_all_data_by_id($id)
	{       
		$this->db->select('*');
		$this->db->from('rbc_banners');
                $this->db->join('rbc_banner_locations','rbc_banners.location_id = rbc_banner_locations.id','left');
                $this->db->join('rbc_category','rbc_banners.category_location = rbc_category.category_id','left');
		$this->db->where('rbc_banners.id', $id);
                $query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function update($id, $item)
	{
             
		$data = array(
			'image_path' => $item['image_path'],
			'location_id' => $item['location_id'],
			'category_location' => $item['category_location'],
			'banner_description' => $item['banner_description']
			 ); 
               
		$this->db->where('id', $id);
		$this->db->update('rbc_banners', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('rbc_banners');
	}
}