<?php
class Rbc_news_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'news_title' => $item['news_title'],
			'news_description' => $item['news_description'],
			'news_date' => $item['news_date'],
			'news_image' => $item['news_image']
			 ); 
               

		$this->db->insert('rbc_news', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('rbc_news');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('rbc_news');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'news_title' => $item['news_title'],
			'news_description' => $item['news_description'],
			'news_date' => $item['news_date'],
			'news_image' => $item['news_image']
			 ); 
                
		$this->db->where('id', $id);
		$this->db->update('rbc_news', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('rbc_news');
	}
}