<?php
class Product_images_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'product_id' => $item['product_id'],
			'image_url' => $item['image_url']
			 ); 

		$this->db->insert('product_images', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('product_images');
		$this->db->where('image_id', $id);
		
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}
        function get_by_product_id($id)
	{
            
		$this->db->select('*');
		$this->db->from('product_images');
		$this->db->where('product_id', $id);
		
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('product_images');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'image_url' => $item['image_url']
			 ); 

		$this->db->where('product_id', $id);
		$this->db->update('product_images', $data);
	}

	function delete($id)
	{
		$this->db->where('image_id', $id);
		$this->db->where('product_id', $id);
		$this->db->delete('product_images');
	}
        function delete_image($id) {
        $this->db->where('image_id', $id);
        $this->db->delete('product_images');
    }
}