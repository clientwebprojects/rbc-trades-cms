<?php
class Member_master_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'member_name' => $item['member_name'],
			'member_type' => $item['member_type'],
			'company_id' => $item['company_id']
			 );

		$this->db->insert('member_master', $data);
                $insert_id = $this->db->insert_id();
                $this->db->trans_complete();
                return  $insert_id;
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('member_master');
		$this->db->where('member_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('member_master');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result_array();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'member_name' => $item['member_name'],
			'member_type' => $item['member_type'],
			'company_id' => $item['company_id']
			 );

		$this->db->where('member_id', $id);
		$this->db->update('member_master', $data);
	}

	function delete($id)
	{
		$this->db->where('member_id', $id);
		$this->db->delete('member_master');
	}
        function get_company_id($id)
	{
		$this->db->select('company_id');
		$this->db->from('member_master');
		$this->db->where('member_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}
}