<?php
class Rbc_uom_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'uom_name' => $item['uom_name'],
			'uom_code' => $item['uom_code']
			 ); 

		$this->db->insert('rbc_uom', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('rbc_uom');
		$this->db->where('uom_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('rbc_uom');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
                    return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'uom_name' => $item['uom_name'],
			'uom_code' => $item['uom_code']
			 ); 

		$this->db->where('uom_id', $id);
		$this->db->update('rbc_uom', $data);
	}

	function delete($id)
	{
		$this->db->where('uom_id', $id);
		$this->db->delete('rbc_uom');
	}
}