<?php
class Rbc_category_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'category_name' => $item['category_name'],
			'parent_id' => $item['parent_id']
			 );

		$this->db->insert('rbc_category', $data);
	}
	function get_category_for_menu()
	{
		$this->db->select('*');
		$this->db->from('rbc_category');
		$this->db->where('parent_id', '0');
                $this->db->where('type', 'P');
                $this->db->order_by("category_name", "ASC");
                $this->db->limit(5);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}
	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('rbc_category');
		$this->db->where('category_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('rbc_category');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

        function get_all_parent_id($id)
	{
		$this->db->select('*');
		$this->db->from('rbc_category');
		$this->db->where('parent_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'category_name' => $item['category_name'],
//			'parent_id' => $item['parent_id']
			 );

		$this->db->where('category_id', $id);
		$this->db->update('rbc_category', $data);
	}
        function get_by_category()
	{
		$this->db->select('*');
		$this->db->from('rbc_category');
		$this->db->where('parent_id', '0');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

        function get_by_sub_category()
	{
		$this->db->select('*');
		$this->db->from('rbc_category');
		$this->db->where('parent_id >', '0');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}
        function get_by_category_service()
	{
		$this->db->select('*');
		$this->db->from('rbc_category');
		$this->db->where('parent_id', '0');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

        function get_by_sub_category_service()
	{
		$this->db->select('*');
		$this->db->from('rbc_category');
		$this->db->where('parent_id >', '0');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function delete($id)
	{
		$this->db->where('category_id', $id);
		$this->db->delete('rbc_category');
	}
        function delete_sub_categories($id)
	{
		$this->db->where('parent_id',$id);
		$this->db->delete('rbc_category');
	}
}


/*
class Rbc_category_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'category_name' => $item['category_name'],
			'parent_id' => $item['parent_id']
			 );

		$this->db->insert('rbc_category', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('rbc_category');
		$this->db->where('category_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('rbc_category');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'category_name' => $item['category_name'],
			'parent_id' => $item['parent_id']
			 );

		$this->db->where('category_id', $id);
		$this->db->update('rbc_category', $data);
	}
        function get_by_category()
	{
		$this->db->select('*');
		$this->db->from('rbc_category');
		$this->db->where('parent_id', NULL);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}
        function get_by_sub_category()
	{
		$this->db->select('*');
		$this->db->from('rbc_category');
		$this->db->where('parent_id', !NULL);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function delete($id)
	{
		$this->db->where('category_id', $id);
		$this->db->delete('rbc_category');
	}
}
  */
