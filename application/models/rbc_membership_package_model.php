<?php
class Rbc_membership_package_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'currency_id' => $item['currency_id'],
			'package_fee' => $item['package_fee'],
			'package_duration' => $item['package_duration']
			 );

		$this->db->insert('rbc_membership_package', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('rbc_membership_package');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('mp.*,c.currency_code,c.currency_symbol,c.id as currency_id');
		$this->db->from('rbc_membership_package mp');
                $this->db->join('rbc_currency c','mp.currency_id = c.id','left');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'currency_id' => $item['currency_id'],
			'package_fee' => $item['package_fee'],
			'package_duration' => $item['package_duration']
			 );

		$this->db->where('id', $id);
		$this->db->update('rbc_membership_package', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('rbc_membership_package');
	}
}