<?php
class Rbc_user_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'id' => $item['id'],
			'user_name' => $item['user_name'],
			'user_password' => $item['user_password']
			 );

		$this->db->insert('rbc_user', $data);
	}

	function get_by_id($id)
	{
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

        function check_user($user,$pwd)
	{
                //exit($user.' '.$pwd);
		$query = $this->db->get_where('rbc_user',array('user_name'=>$user,'user_password'=>  md5($pwd)));

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('rbc_user');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}
        function get_active_users()
	{
		$this->db->select('*');
		$this->db->from('rbc_user');
                $this->db->join('member_master','rbc_user.member_id = member_master.member_id','left');
                $this->db->join('company_master','member_master.company_id = company_master.id','left');
		$this->db->where('is_active', 0);
                $query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

        function get_banned_users()
	{
		$this->db->select('*');
		$this->db->from('rbc_user');
                $this->db->join('member_master','rbc_user.member_id = member_master.member_id','left');
                $this->db->join('company_master','member_master.company_id = company_master.id','left');
		$this->db->where('is_active', 0);
                $query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

        function get_all_users()
	{
		$this->db->select('COUNT(user_id) AS total');
		$this->db->from('rbc_user');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}
	function update($id, $item)
	{
		$data = array(

			'is_active' => '1'
			 );

		$this->db->where('user_id', $id);
		$this->db->update('rbc_user', $data);
	}
        function update_unban($id, $item)
	{
		$data = array(

			'is_active' => '0'
			 );

		$this->db->where('user_id', $id);
		$this->db->update('rbc_user', $data);
	}

	function delete($id)
	{
		$this->db->delete('rbc_user');
	}
}