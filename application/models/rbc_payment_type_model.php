<?php
class Rbc_payment_type_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'p_term_description' => $item['p_term_description']
			 ); 

		$this->db->insert('rbc_payment_type', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('rbc_payment_type');
		$this->db->where('p_term_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('rbc_payment_type');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'p_term_description' => $item['p_term_description']
			 ); 

		$this->db->where('p_term_id', $id);
		$this->db->update('rbc_payment_type', $data);
	}

	function delete($id)
	{
		$this->db->where('p_term_id', $id);
		$this->db->delete('rbc_payment_type');
	}
}