<?php
class Rbc_banner_locations_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'banner_location' => $item['banner_location'],
			'location_description' => $item['location_description']
			 ); 

		$this->db->insert('rbc_banner_locations', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('rbc_banner_locations');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('rbc_banner_locations');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'banner_location' => $item['banner_location'],
			'location_description' => $item['location_description']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('rbc_banner_locations', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('rbc_banner_locations');
	}
}