<?php
class Product_master_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item) {
        $data = array(
            'product_category' => $item['product_category'],
            'product_code' => $item['product_model'],
            'product_model' => $item['product_model'],
            'brand_id' => $item['brand_id'],
            'product_name' => $item['product_name'],
            'product_description' => $item['editor1'],
            'product_keyword' => $item['product_keyword'],
            'uom_id' => $item['uom_id'],
            'product_origin' => $item['product_origin'],
            'product_type' => $item['product_type'],
            'product_use' => $item['product_use'],
            'product_minimun_order' => $item['product_minimun_order'],
            'product_port' => $item['product_port'],
            'product_payment_type' => $item['product_payment_type'],
            'product_supply_ability' => $item['product_supply_ability'],
            'product_delivery_time' => $item['product_delivery_time'],
            'packaging_detail' => $item['packaging_detail'],
            'product_detailed_description' => $item['product_detailed_description'],
            'product_fob_price' => $item['product_fob_price']
        );
        //print_r($item);exit();
        $this->db->insert('product_master', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }


        function new_create($item) {
        $data = array(
            'product_category' => $item['product_category'],
            'product_code' => $item['product_model'],
            'product_model' => $item['product_model'],
            'brand_id' => $item['brand_id'],
            'images' => "1",
            'product_name' => $item['product_name'],
            'product_description' => $item['editor1'],
            'product_keyword' => $item['product_keyword'],
            'uom_id' => "1",
            'product_status' => '0',
            'product_origin' => $item['product_origin'],
            'product_type' => $item['product_type'],
            'product_use' => $item['product_use'],
            'product_minimun_order' => $item['product_minimun_order'],
            'product_port' => $item['product_port'],
            'product_payment_type' => $item['product_payment_type'],
            'product_supply_ability' => $item['product_supply_ability'],
            'product_delivery_time' => $item['product_delivery_time'],
            'packaging_detail' => $item['packaging_detail'],
            'product_detailed_description' => $item['product_detailed_description'],
            'product_fob_price' => $item['product_fob_price']
        );
        //print_r($item);exit();
        $this->db->insert('product_master', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('product_master');
		$this->db->where('product_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}
        function get_all_pending_products()
	{
		$this->db->select('*');
		$this->db->from('product_master');
                $this->db->where('product_status', 'P');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

        function get_products_by_status($type,$status)
	{
		$this->db->select('*');
		$this->db->from('product_master p');
                $this->db->join('product_member pm','p.product_id = pm.product_id','left');
		$this->db->where('p.product_status', $status);
                $this->db->where('pm.type', $type);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}
        function get_all_pending_admin_products($id)
	{
		$this->db->select('*');
		$this->db->from('product_master');
                $this->db->join('product_member','product_master.product_id = product_member.product_id','left');
		$this->db->where('product_member.member_id', $id);
                $this->db->where('product_status', 'P');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

        function get_all_active_admin_products($id)
	{
		$this->db->select('*');
		$this->db->from('product_master');
                $this->db->join('product_member','product_master.product_id = product_member.product_id','left');
		$this->db->where('product_member.member_id', $id);
		$this->db->where('product_status', 'A');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item) {
        $data = array(
            'product_category' => $item['product_category'],
            'product_code' => $item['product_code'],
            'product_model' => $item['product_model'],
            'brand_id' => $item['brand_id'],
            'product_name' => $item['product_name'],
            'product_description' => $item['product_description'],
            'product_keyword' => $item['product_keyword'],
            'uom_id' => $item['uom_id'],
            'product_status' => 'P',
            'product_origin' => $item['product_origin'],
            'product_type' => $item['product_type'],
            'product_use' => $item['product_use'],
            'product_minimun_order' => $item['product_minimun_order'],
            'product_port' => $item['product_port'],
            'product_payment_type' => $item['product_payment_type'],
            'product_supply_ability' => $item['product_supply_ability'],
            'product_delivery_time' => $item['product_delivery_time'],
            'packaging_detail' => $item['packaging_detail'],
            'product_detailed_description' => $item['product_detailed_description'],
            'product_fob_price' => $item['product_fob_price']
        );
 //print_r($data);exit();
        $this->db->where('product_id', $id);
        $this->db->update('product_master', $data);
    }

	function delete($id)
	{
		$this->db->where('product_id', $id);
		$this->db->delete('product_master');
	}
        function product_approve($id)
        {
            $data = array(
                'product_status' => 'A'
            );
            $this->db->where('product_id', $id);
            $this->db->update('product_master', $data);
        }

        function product_reject($id)
        {
            $data = array(
                'product_status' => 'R'
            );
            $this->db->where('product_id', $id);
            $this->db->update('product_master', $data);
        }
        function product_pending($id)
        {
            $data = array(
                'product_status' => 'P'
            );
            $this->db->where('product_id', $id);
            $this->db->update('product_master', $data);
        }
        function get_count_pending_products()
	{
		$this->db->select('COUNT(product_master.product_id) AS total');
		$this->db->from('product_master');
                $this->db->join('product_member','product_master.product_id = product_member.product_id','left');
                $this->db->where('product_master.product_status', 'P');
                $this->db->where('product_member.type', 'P');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}
        function get_count_pending_services()
	{
		$this->db->select('COUNT(service_master.service_id) AS total');
		$this->db->from('service_master');
                $this->db->join('product_member','service_master.service_id = product_member.product_id','left');
                $this->db->where('service_master.service_status', 'P');
                $this->db->where('product_member.type', 'S');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}
        function get_count_active_services()
	{
		$this->db->select('COUNT(service_master.service_id) AS total');
		$this->db->from('service_master');
                $this->db->join('product_member','service_master.service_id = product_member.product_id','left');
                $this->db->where('service_master.service_status', 'A');
                $this->db->where('product_member.type', 'S');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

}