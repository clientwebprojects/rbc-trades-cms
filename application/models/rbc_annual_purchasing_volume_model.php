<?php
class Rbc_annual_purchasing_volume_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'annual_purchasing_volume' => $item['annual_purchasing_volume']
			 ); 

		$this->db->insert('rbc_annual_purchasing_volume', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('rbc_annual_purchasing_volume');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('rbc_annual_purchasing_volume');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result_array();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'annual_purchasing_volume' => $item['annual_purchasing_volume']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('rbc_annual_purchasing_volume', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('rbc_annual_purchasing_volume');
	}
}