<?php
class Rbc_countries_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
                        'country_name' => $item['country_name']
			 );
		$this->db->insert('rbc_countries', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('rbc_countries');
		$this->db->where('country_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('rbc_countries');
                $this->db->order_by("country_name", "ASC");
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();

		}
	}

        function get_all_country_names(){
                $this->db->select('*');
		$this->db->from('rbc_countries');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result_array();

		}
        }

	function update($id, $item)
	{
		$data = array(
                        'country_name'           => $item['country_name']
			 );

		$this->db->where('country_id', $id);
		$this->db->update('rbc_countries', $data);
	}


	function delete($id)
	{
		$this->db->where('country_id', $id);
		$this->db->delete('rbc_countries');
	}
}